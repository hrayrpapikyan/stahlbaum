<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
abstract class MY_Controller extends CI_Controller {
    function __construct()
    {
        parent::__construct();

        $this->load->model('admin_user_model');
        $this->load->model('options_model');
        $this->load->model('paymentandshipping_model');
        $this->load->model('categories_model');

        $this->load->model('products_model');
							

        $this->load->model('staticpages_model');
        $this->load->model('slider_model');
        if(!$this->admin_user_model->isLogedIn()){
            redirect('admin_login/logout');
        }
        $this->load->library('form_validation');

    }
    protected function render($contentView=false,$title=false,$pageTitle=false,$pageSubTitle='',$formTitle='',$data=array())
    {
					

        $_title=($title)?$title:$this->config->item('project_name');
        $_pageTitle=($pageTitle)?$pageTitle:$this->config->item('project_name');
        $this->load->view('admin/common/header',array('title'=>$_title));
        $this->load->view('admin/common/top_menu',array('project_name'=>$_pageTitle));

        $this->load->view('admin/common/left_menu',array('categories'=>$this->categories_model->getAllCategories(),
            'categoriesWithProducts'=>$this->products_model->getCategoriesWithProducts()));

        if($contentView){
            $data['pageSubTitle']=$pageSubTitle;
            $data['formTitle']=$formTitle;
            $this->load->view($contentView,$data);
        }
        $this->load->view('admin/common/footer');
    }
    protected function staticPages($dbkey,$pageTitle){
        if(!$this->input->post('pagecontent')){
            $pageContent=$this->staticpages_model->getPageContent($dbkey);
            $this->render('admin/pagecontent','',"",$pageTitle,"Введите пожалуйста данные",array('pageContent'=>$pageContent,'actionName'=>$dbkey));
        }
        else{
            $this->staticpages_model->editPageContent($dbkey,$this->input->post('pagecontent'));
            $this->render('admin/common/success','',"",$pageTitle,"",array('scsMsg' => 'Раздел удачно отредактирован'));
        }
    }
    private function random()
    {
        $zn = microtime();
        $zn = md5($zn);
        return substr($zn, 2, 7);
    }
    protected function set_img_nf($f)
    {
        return $this->random().".".$f;
    }


}