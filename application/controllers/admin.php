<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'controllers/MY_Controller.php');
class Admin extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $this->render('admin/options', '', "", "Настройки", "Редактирование настроек", array('options' => $this->options_model->getOptions()));
    }
//static pages
    public function aboutus()
    {
        $this->staticPages('aboutus', "О Нас");
    }
    public function tocustomers()
    {
        $this->staticPages('tocustomers', "Покупателям");
    }
    public function shipping()
    {
        $this->staticPages('shipping', "Оплата и Доставка");
    }
//end static pages
    public function settings()
    {
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('passwordCurrent', 'Действующий пароль', 'required');
        $this->form_validation->set_rules('password', 'Новый пароль', 'required');
        $this->form_validation->set_rules('passwordConfirm', 'Повтор пороля', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/settings', '', "", "Изменить пароль", "Введите пожалуйста новый логин и пароль");
        } else {
            $this->render('admin/settings', '', "", "Изменить пароль", "Введите пожалуйста новый логин и пароль", array('scsMsg' => "Ваши данные измемены"));
        }
    }

    public function editCategory($categoryId)
    {
        $this->form_validation->set_rules('category_name', 'Имя категории', 'required');
        //$this->form_validation->set_rules('diameters', 'Диаметры', 'required');
        $this->form_validation->set_rules('slug_name', 'слэг', 'required');
        $categoryInfo = $this->categories_model->getCategory($categoryId);
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/editcategory', '', "", "Редактировать категорию", "Введите пожалуйста данные категории", array('categoryInfo' => $categoryInfo));
        } else {
            if (isset($_FILES['userfile']['name']) && !empty($_FILES['userfile']['tmp_name'])) {
                $config['upload_path'] = './uploads/categories';
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['max_size'] = '6048';
                $ext = end(explode(".", $_FILES['userfile']['name']));
                $config['file_name'] = $this->set_img_nf($ext);
                //echo $ext; die;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    $this->render('admin/editcategory', '', "", "Редактировать категорию", "Введите пожалуйста данные категории", array('error' => $this->upload->display_errors()));
                } else {
                    if ($categoryInfo['thumb'] != '' && file_exists(FCPATH . 'uploads/categories/' . $categoryInfo['thumb'])) {
                        unlink(FCPATH . 'uploads/categories/' . $categoryInfo['thumb']);
                    }
                    $config1['image_library'] = 'gd2';
                    $config1['source_image'] = './uploads/categories/' . $config['file_name'];
                    $config1['create_thumb'] = FALSE;
                    $config1['maintain_ratio'] = TRUE;
                    $config1['width'] = 267;
                    $config1['height'] = 267;
                    $this->load->library('image_lib', $config1);
                    $this->image_lib->resize();
                }
            }

            $this->categories_model->editCategory($categoryId, $this->input->post('category_name'), $this->input->post('slug_name'), $this->input->post('description'), (isset($config)) ? $config['file_name'] : "", $this->input->post('diameters'));
            $this->render('admin/common/success', '', "", "Редактировать категорию", "", array('scsMsg' => 'Категория удачно отредактирована'));
        }
    }


    public function editcategorynew($categoryId)
    {
        $this->form_validation->set_rules('category_name', 'Имя категории', 'required');
        //$this->form_validation->set_rules('diameters', 'Диаметры', 'required');
        $this->form_validation->set_rules('slug_name', 'слэг', 'required');
        $categoryInfo = $this->categories_model->getCategoryNew($categoryId);
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/editcategorynew', '', "", "Редактировать категорию", "Введите пожалуйста данные категории", array('categoryInfo' => $categoryInfo));
        } else {
            if (isset($_FILES['userfile']['name']) && !empty($_FILES['userfile']['tmp_name'])) {
                $config['upload_path'] = './uploads/categories';
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['max_size'] = '6048';
                $ext = end(explode(".", $_FILES['userfile']['name']));
                $config['file_name'] = $this->set_img_nf($ext);
                //echo $ext; die;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    $this->render('admin/editcategorynew', '', "", "Редактировать категорию", "Введите пожалуйста данные категории", array('error' => $this->upload->display_errors()));
                } else {
                    if ($categoryInfo['thumb'] != '' && file_exists(FCPATH . 'uploads/categories/' . $categoryInfo['thumb'])) {
                        unlink(FCPATH . 'uploads/categories/' . $categoryInfo['thumb']);
                    }
                    $config1['image_library'] = 'gd2';
                    $config1['source_image'] = './uploads/categories/' . $config['file_name'];
                    $config1['create_thumb'] = FALSE;
                    $config1['maintain_ratio'] = TRUE;
                    $config1['width'] = 267;
                    $config1['height'] = 267;
                    $this->load->library('image_lib', $config1);
                    $this->image_lib->resize();
                }
            }

            $this->categories_model->editCategoryNew($categoryId, $this->input->post('category_name'), $this->input->post('slug_name'), $this->input->post('description'), (isset($config)) ? $config['file_name'] : "", $this->input->post('diameters'));
            $this->render('admin/common/success', '', "", "Редактировать категорию", "", array('scsMsg' => 'Категория удачно отредактирована'));
        }
    }


    public function deleterelation(){
        $this->categories_model->deleterelation($this->input->post('relation_id'),$this->input->post('diamt'));
        exit;
    }

    public function deleterelationnew(){
        $this->categories_model->deleterelationnew($this->input->post('relation_id'),$this->input->post('diamt'));
        exit;
    }

    public function deletecategory()
    {
        $this->categories_model->deleteCategory($this->input->post('category_id'));
        exit;
    }
    public function deletecategorynew()
    {
        $this->categories_model->deleteCategoryNew($this->input->post('category_id'));
        exit;
    }

    public function deletecategorysimple()
    {
        $this->categories_model->deleteCategorySimple($this->input->post('category_id'));
        exit;
    }

    public function deleteProduct()
    {
        $this->products_model->deleteProduct($this->input->post('product_id'));
        exit;
    }

    public function deleteproductnew()
    {
        $this->products_model->deleteProductNew($this->input->post('product_id'));
        exit;
    }

    public function options()
    {
        $this->form_validation->set_rules('tel1', 'Первый телефонный номер', 'required');
        $this->form_validation->set_rules('title', 'Имя сайта', 'required');
        $this->form_validation->set_rules('meta_desc', 'Meta Description', 'required');
        $this->form_validation->set_rules('meta_keyw', 'Meta Keyword', 'required');
        $this->form_validation->set_rules('address1', 'Первый адрес', 'required');
        $this->form_validation->set_rules('contacts_email', 'Контактный email', 'required|valid_email');
        $this->form_validation->set_rules('sales_email1', 'email для заказов', 'valid_email|required');
        $this->form_validation->set_rules('map', 'карта', 'required');
        $this->form_validation->set_rules('footer_text', 'Текст Футера', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/options', '', "", "Настройки", "Редактирование настроек", array('options' => $this->options_model->getOptions()));
        } else {
            $this->options_model->editOptions($this->input->post('tel1'), $this->input->post('tel2'), $this->input->post('address1'), $this->input->post('address2'), $this->input->post('contacts_email'), $this->input->post('sales_email1'),  $this->input->post('map'), $this->input->post('footer_text'),$this->input->post('meta_keyw'),$this->input->post('meta_desc'),$this->input->post('title'));
            $this->render('admin/common/success', '', "", "Настройки", "", array('scsMsg' => 'Настройки удачно отредактированы'));
        }
    }

    public function addCategory($superCategoryId)
    {
        $this->form_validation->set_rules('category_name', 'Имя подкатегории', 'required');
        $this->form_validation->set_rules('diameters', 'Диаметры', 'required');
        $this->form_validation->set_rules('slug_name', 'слэг', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/addcategory', '', "", "Добавить подкатегорию с диаметрамы и материаламы", "Введите пожалуйста данные новой категории",array('superCategoryId'=>$superCategoryId));
        } else {
            if (isset($_FILES['userfile']['name']) && !empty($_FILES['userfile']['tmp_name'])) {
                $config['upload_path'] = './uploads/categories';
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['max_size'] = '6048';
                $ext = end(explode(".", $_FILES['userfile']['name']));
                $config['file_name'] = $this->set_img_nf($ext);
                //echo $ext; die;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    $this->render('admin/addcategory', '', "", "Добавить подкатегорию с диаметрамы и материаламы", "Введите пожалуйста данные новой категории", array('error' => $this->upload->display_errors(),'superCategoryId'=>$superCategoryId));
                } else {
                    $config1['image_library'] = 'gd2';
                    $config1['source_image'] = './uploads/categories/' . $config['file_name'];
                    $config1['create_thumb'] = FALSE;
                    $config1['maintain_ratio'] = TRUE;
                    $config1['width'] = 267;
                    $config1['height'] = 267;
                    $this->load->library('image_lib', $config1);
                    $this->image_lib->resize();
                }
            }
            $this->categories_model->addCategory($this->input->post('category_name'), $this->input->post('slug_name'), $this->input->post('description'), (isset($config)) ? $config['file_name'] : "", $this->input->post('diameters'),$superCategoryId);
            $this->render('admin/common/success', '', "", "Добавить подкатегорию с диаметрамы и материаламы", "", array('scsMsg' => 'Подкатегория удачно добавлена'));
        }
    }


    public function addcategorynew($superCategoryId)
    {
        $this->form_validation->set_rules('category_name', 'Имя подкатегории', 'required');
        $this->form_validation->set_rules('diameters', 'Диаметры', 'required');
        $this->form_validation->set_rules('slug_name', 'слэг', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/addcategorynew', '', "", "Добавить подкатегорию с диаметрамы но без материалов", "Введите пожалуйста данные новой категории",array('superCategoryId'=>$superCategoryId));
        } else {
            if (isset($_FILES['userfile']['name']) && !empty($_FILES['userfile']['tmp_name'])) {
                $config['upload_path'] = './uploads/categories';
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['max_size'] = '6048';
                $ext = end(explode(".", $_FILES['userfile']['name']));
                $config['file_name'] = $this->set_img_nf($ext);
                //echo $ext; die;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    $this->render('admin/addcategorynew', '', "", "Добавить подкатегорию с диаметрамы и материаламы", "Введите пожалуйста данные новой категории", array('error' => $this->upload->display_errors(),'superCategoryId'=>$superCategoryId));
                } else {
                    $config1['image_library'] = 'gd2';
                    $config1['source_image'] = './uploads/categories/' . $config['file_name'];
                    $config1['create_thumb'] = FALSE;
                    $config1['maintain_ratio'] = TRUE;
                    $config1['width'] = 267;
                    $config1['height'] = 267;
                    $this->load->library('image_lib', $config1);
                    $this->image_lib->resize();
                }
            }
            $this->categories_model->addCategoryNew($this->input->post('category_name'), $this->input->post('slug_name'), $this->input->post('description'), (isset($config)) ? $config['file_name'] : "", $this->input->post('diameters'),$superCategoryId);
            $this->render('admin/common/success', '', "", "Добавить подкатегорию с диаметрамы но без материалов", "", array('scsMsg' => 'Подкатегория удачно добавлена'));
        }
    }

    public function addproduct($category_id){

        $this->form_validation->set_rules('slugname', 'слэг', 'required');

        $this->form_validation->set_rules('product_name', 'Имя Продукта', 'required');
        if ($this->form_validation->run() == FALSE) {
            $categoryInfo = $this->categories_model->getCategory($category_id);
            $this->render('admin/addproduct', '', "", "Добавить продукт к категории - ".$categoryInfo['title'], "Введите пожалуйста данные нового продукта", array('categoryInfo'=>$categoryInfo) );
        }
        else{
        if (isset($_POST['submit']))
        {
         //  echo '<pre>'; print_r($_POST); die;

            $this->load->library('upload');


            foreach($_FILES as $field => $file)
            {

                        if ($field == "userfile"  && !empty($file['tmp_name'])){
                        $config=array();
                        $config['upload_path'] = './uploads/products/thumb';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['max_size'] = '6048';
                        $ext = end(explode(".", $file['name']));
                        $config['file_name'] = $this->set_img_nf($ext);
                        $this->upload->initialize($config);
                        if($file['error'] == 0)
                        {
                            if ($this->upload->do_upload($field))
                            {
                                $config_img['image_library'] = 'gd2';
                                $config_img['source_image'] = './uploads/products/thumb/' . $config['file_name'];
                                $config_img['create_thumb'] = FALSE;
                                $config_img['maintain_ratio'] = TRUE;
                                $config_img['width'] = 204;
                                $config_img['height'] = 204;
                                $this->load->library('image_lib', $config_img);
                                $this->image_lib->resize();
                            }
                            else
                            {
                                $categoryInfo = $this->categories_model->getCategory($category_id);
                                $this->render('admin/addproduct', '', "", "Добавить продукт к категории - ".$categoryInfo['title'], "Введите пожалуйста данные нового продукта", array('categoryInfo'=>$categoryInfo,'userfileerror'=>$this->upload->display_errors()) );
                            }
                        }
                        }
                        if ($field == "userfile1" && !empty($file['tmp_name'])){
                        $config1=array();
                        $config1['upload_path'] = './uploads/products/popup';
                        $config1['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config1['max_size'] = '6048';
                            $ext = end(explode(".", $file['name']));
                        $config1['file_name'] = $this->set_img_nf($ext);
                        $this->upload->initialize($config1);

                    if($file['error'] == 0)
                        {
                            // So lets upload
                            if ($this->upload->do_upload($field))
                            {
                                $data = $this->upload->data();
                            }
                            else
                            {
                                $categoryInfo = $this->categories_model->getCategory($category_id);
                                $this->render('admin/addproduct', '', "", "Добавить продукт к категории - ".$categoryInfo['title'], "Введите пожалуйста данные нового продукта", array('categoryInfo'=>$categoryInfo,'userfile1error'=>$this->upload->display_errors()) );
                            }
                          }
                }
                // No problems with the file

            }


        }
            $this->products_model->addProduct($category_id,$this->input->post('product_name'),$this->input->post('slugname'),(isset($config))?$config['file_name']:"",(isset($config1))?$config1['file_name']:"",$this->input->post('mataerial_long'),$this->input->post('material_short'),$this->input->post('material_price'));
            $this->render('admin/common/success', '', "", "Добавить продукт", "", array('scsMsg' => 'Продукт удачно добавлен'));
    }
    }

    public function editproduct($product_id){
        $productInfo = $this->products_model->getProductByID($product_id);

        $this->form_validation->set_rules('slugname', 'слэг', 'required');

        $this->form_validation->set_rules('product_name', 'Имя Продукта', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/editproduct', '', "", "Редактировать продукт", "Введите пожалуйста данные  продукта", array('productInfo'=>$productInfo) );
        }
        else{

            if (isset($_POST['submit']))
            {
                $this->load->library('upload');


                foreach($_FILES as $field => $file)
                {

                    if ($field == "userfile" && !empty($file['tmp_name'])){
                        $config=array();
                        $config['upload_path'] = './uploads/products/thumb';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['max_size'] = '6048';
                        $ext = end(explode(".", $file['name']));
                        $config['file_name'] = $this->set_img_nf($ext);
                        $this->upload->initialize($config);
                        if($file['error'] == 0)
                        {
                            if ($this->upload->do_upload($field))
                            {
                                if ($productInfo['product']->img_main != '' && file_exists(FCPATH . 'uploads/products/thumb/' . $productInfo['product']->img_main)) {
                                    unlink(FCPATH . 'uploads/products/thumb/' . $productInfo['product']->img_main);
                                }

                                $config_img['image_library'] = 'gd2';
                                $config_img['source_image'] = './uploads/products/thumb/' . $config['file_name'];
                                $config_img['create_thumb'] = FALSE;
                                $config_img['maintain_ratio'] = TRUE;
                                $config_img['width'] = 204;
                                $config_img['height'] = 204;
                                $this->load->library('image_lib', $config_img);
                                $this->image_lib->resize();
                            }
                            else
                            {

                                $this->render('admin/editproduct', '', "", "Редактировать продукт", "Введите пожалуйста данные  продукта", array('productInfo'=>$productInfo, 'userfileerror'=>$this->upload->display_errors()) );
                            }
                        }
                    }
                    if ($field == "userfile1" && !empty($file['tmp_name'])){
                        $config1=array();
                        $config1['upload_path'] = './uploads/products/popup';
                        $config1['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config1['max_size'] = '6048';
                        $ext = end(explode(".", $file['name']));
                        $config1['file_name'] = $this->set_img_nf($ext);
                        $this->upload->initialize($config1);

                        if($file['error'] == 0)
                        {
                            // So lets upload
                            if ($this->upload->do_upload($field))
                            {
                                if ($productInfo['product']->img_more != '' && file_exists(FCPATH . 'uploads/products/thumb/' . $productInfo['product']->img_more)) {
                                    unlink(FCPATH . 'uploads/products/popup/' . $productInfo['product']->img_more);
                                }
                            }
                            else
                            {
                                $this->render('admin/editproduct', '', "", "Редактировать продукт", "Введите пожалуйста данные  продукта", array('productInfo'=>$productInfo, 'userfileerror1'=>$this->upload->display_errors()) );
                            }
                        }
                    }
                    // No problems with the file

                }


            }
            $this->products_model->editProduct($product_id,$this->input->post('product_name'),$this->input->post('slugname'),(isset($config))?$config['file_name']:"",(isset($config1))?$config1['file_name']:"",$this->input->post('mataerial_long'),$this->input->post('material_short'),$this->input->post('material_price'),$this->input->post('mataerial_id_keeper'));
            $this->render('admin/common/success', '', "", "Редактировать продукт", "", array('scsMsg' => 'Продукт удачно отредактирован'));
        }
    }


    public function addproductnew($category_id){

        $this->form_validation->set_rules('slugname', 'слэг', 'required');

        $this->form_validation->set_rules('product_name', 'Имя Продукта', 'required');
        if ($this->form_validation->run() == FALSE) {
            $categoryInfo = $this->categories_model->getCategoryNew($category_id);
            $this->render('admin/addproductnew', '', "", "Добавить продукт к категории - ".$categoryInfo['title'], "Введите пожалуйста данные нового продукта", array('categoryInfo'=>$categoryInfo) );
        }
        else{
            if (isset($_POST['submit']))
            {
                //  echo '<pre>'; print_r($_POST); die;

                $this->load->library('upload');


                foreach($_FILES as $field => $file)
                {

                    if ($field == "userfile"  && !empty($file['tmp_name'])){
                        $config=array();
                        $config['upload_path'] = './uploads/products/thumb';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['max_size'] = '6048';
                        $ext = end(explode(".", $file['name']));
                        $config['file_name'] = $this->set_img_nf($ext);
                        $this->upload->initialize($config);
                        if($file['error'] == 0)
                        {
                            if ($this->upload->do_upload($field))
                            {
                                $config_img['image_library'] = 'gd2';
                                $config_img['source_image'] = './uploads/products/thumb/' . $config['file_name'];
                                $config_img['create_thumb'] = FALSE;
                                $config_img['maintain_ratio'] = TRUE;
                                $config_img['width'] = 204;
                                $config_img['height'] = 204;
                                $this->load->library('image_lib', $config_img);
                                $this->image_lib->resize();
                            }
                            else
                            {
                                $categoryInfo = $this->categories_model->getCategoryNew($category_id);
                                $this->render('admin/addproductnew', '', "", "Добавить продукт к категории - ".$categoryInfo['title'], "Введите пожалуйста данные нового продукта", array('categoryInfo'=>$categoryInfo,'userfileerror'=>$this->upload->display_errors()) );
                            }
                        }
                    }
                    if ($field == "userfile1" && !empty($file['tmp_name'])){
                        $config1=array();
                        $config1['upload_path'] = './uploads/products/popup';
                        $config1['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config1['max_size'] = '6048';
                        $ext = end(explode(".", $file['name']));
                        $config1['file_name'] = $this->set_img_nf($ext);
                        $this->upload->initialize($config1);

                        if($file['error'] == 0)
                        {
                            // So lets upload
                            if ($this->upload->do_upload($field))
                            {
                                $data = $this->upload->data();
                            }
                            else
                            {
                                $categoryInfo = $this->categories_model->getCategoryNew($category_id);
                                $this->render('admin/addproduct', '', "", "Добавить продукт к категории - ".$categoryInfo['title'], "Введите пожалуйста данные нового продукта", array('categoryInfo'=>$categoryInfo,'userfile1error'=>$this->upload->display_errors()) );
                            }
                        }
                    }
                    // No problems with the file

                }


            }
            $this->products_model->addProductNew($category_id,$this->input->post('product_name'),$this->input->post('slugname'),(isset($config))?$config['file_name']:"",(isset($config1))?$config1['file_name']:"",$this->input->post('diameter_price'));
            $this->render('admin/common/success', '', "", "Добавить продукт", "", array('scsMsg' => 'Продукт удачно добавлен'));
        }
    }

    public function editproductnew($product_id){
        $productInfo = $this->products_model->getProductByIDNew($product_id);

        $this->form_validation->set_rules('slugname', 'слэг', 'required');

        $this->form_validation->set_rules('product_name', 'Имя Продукта', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/editproductnew', '', "", "Редактировать продукт", "Введите пожалуйста данные  продукта", array('productInfo'=>$productInfo) );
        }
        else{

            if (isset($_POST['submit']))
            {
                $this->load->library('upload');


                foreach($_FILES as $field => $file)
                {

                    if ($field == "userfile" && !empty($file['tmp_name'])){
                        $config=array();
                        $config['upload_path'] = './uploads/products/thumb';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['max_size'] = '6048';
                        $ext = end(explode(".", $file['name']));
                        $config['file_name'] = $this->set_img_nf($ext);
                        $this->upload->initialize($config);
                        if($file['error'] == 0)
                        {
                            if ($this->upload->do_upload($field))
                            {
                                if ($productInfo['product']->img_main != '' && file_exists(FCPATH . 'uploads/products/thumb/' . $productInfo['product']->img_main)) {
                                    unlink(FCPATH . 'uploads/products/thumb/' . $productInfo['product']->img_main);
                                }

                                $config_img['image_library'] = 'gd2';
                                $config_img['source_image'] = './uploads/products/thumb/' . $config['file_name'];
                                $config_img['create_thumb'] = FALSE;
                                $config_img['maintain_ratio'] = TRUE;
                                $config_img['width'] = 204;
                                $config_img['height'] = 204;
                                $this->load->library('image_lib', $config_img);
                                $this->image_lib->resize();
                            }
                            else
                            {

                                $this->render('admin/editproductnew', '', "", "Редактировать продукт", "Введите пожалуйста данные  продукта", array('productInfo'=>$productInfo, 'userfileerror'=>$this->upload->display_errors()) );
                            }
                        }
                    }
                    if ($field == "userfile1" && !empty($file['tmp_name'])){
                        $config1=array();
                        $config1['upload_path'] = './uploads/products/popup';
                        $config1['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config1['max_size'] = '6048';
                        $ext = end(explode(".", $file['name']));
                        $config1['file_name'] = $this->set_img_nf($ext);
                        $this->upload->initialize($config1);

                        if($file['error'] == 0)
                        {
                            // So lets upload
                            if ($this->upload->do_upload($field))
                            {
                                if ($productInfo['product']->img_more != '' && file_exists(FCPATH . 'uploads/products/thumb/' . $productInfo['product']->img_more)) {
                                    unlink(FCPATH . 'uploads/products/popup/' . $productInfo['product']->img_more);
                                }
                            }
                            else
                            {
                                $this->render('admin/editproductnew', '', "", "Редактировать продукт", "Введите пожалуйста данные  продукта", array('productInfo'=>$productInfo, 'userfileerror1'=>$this->upload->display_errors()) );
                            }
                        }
                    }
                    // No problems with the file

                }


            }
            $this->products_model->editProductNew($product_id,$this->input->post('product_name'),$this->input->post('slugname'),(isset($config))?$config['file_name']:"",(isset($config1))?$config1['file_name']:"",$this->input->post('diameter_price'));
            $this->render('admin/common/success', '', "", "Редактировать продукт", "", array('scsMsg' => 'Продукт удачно отредактирован'));
        }
    }

    public function cartshipping(){
        if(!isset($_POST['submit'])){

        $this->render('admin/cartshipping', '', "", "Настройки доставки", "Введите пожалуйста данные  доставки", array('shippingData'=> $this->paymentandshipping_model->getShippingData()) );
        }
        else{
            $this->paymentandshipping_model->updateShipping($this->input->post('title'),$this->input->post('price'),$this->input->post('description'));
            $this->render('admin/common/success', '', "", "Настройки доставки", "", array('scsMsg' => 'Настройки удачно отредактированы'));
        }
    }

    public function cartpayment(){
        if(!isset($_POST['submit'])){

            $this->render('admin/cartpayment', '', "", "Настойки платежа", "Введите пожалуйста данные  платежа", array('paymentData'=> $this->paymentandshipping_model->getPaymentData()) );
        }
        else{
            $this->paymentandshipping_model->updatePayment($this->input->post('title'),$this->input->post('price'),$this->input->post('description'));
            $this->render('admin/common/success', '', "", "Настойки платежа", "", array('scsMsg' => 'Настройки удачно отредактированы'));
        }
    }

    public function removematerialedit(){
        $this->products_model->removematerialedit($this->input->post('product_id'),$this->input->post('material_id'));
        exit;
    }

    public function getBodyForNewMaterialEdit(){
        echo $this->products_model->getBodyForNewMaterialEdit($this->input->post('product_id'));
        exit;
    }


    public function slider(){

        $this->render('admin/slider', '', "", "Слайдер", "", array('allSlides'=> $this->slider_model->getAll()) );
    }

    public function addslider()
    {
        $this->form_validation->set_rules('title', 'Имя слайда', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/addslider', '', "", "Добавить ", "Введите пожалуйста данные нового слайда");
        } else {
            if (isset($_FILES['userfile']['name']) && !empty($_FILES['userfile']['tmp_name'])) {
                $config['upload_path'] = './uploads/slider';
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['max_size'] = '10000';
                $ext = end(explode(".", $_FILES['userfile']['name']));
                $config['file_name'] = $this->set_img_nf($ext);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    $this->render('admin/addslider', '', "", "Добавить ", "Введите пожалуйста данные нового слайда", array('error' => $this->upload->display_errors()));
                } else {
                    $config1['image_library'] = 'gd2';
                    $config1['source_image'] = './uploads/slider/' . $config['file_name'];
                    $config1['create_thumb'] = FALSE;
                    $config1['maintain_ratio'] = TRUE;
                    $config1['width'] = 725;
                    $config1['height'] = 280;
                    $this->load->library('image_lib', $config1);
                    $this->image_lib->resize();
                }
            }
            $this->slider_model->addSlider($this->input->post('title'), $this->input->post('subtitle'), $this->input->post('description'), (isset($config)) ? $config['file_name'] : "");
            $this->render('admin/common/success', '', "", "Добавить ", "", array('scsMsg' => 'Слайд удачно добавлен'));
        }
    }

    public function deleteslider(){
        $this->slider_model->deleteslider($this->input->post('slider_id')); exit;
    }

    public function editslider($sliderId)
    {
        $this->form_validation->set_rules('title', 'Имя слайда', 'required');
        $sliderInfo = $this->slider_model->getsliderById($sliderId);
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/editslider', '', "", "Редактировать ", "Введите пожалуйста данные слайда", array('sliderInfo' => $sliderInfo));
        } else {
            if (isset($_FILES['userfile']['name']) && !empty($_FILES['userfile']['tmp_name'])) {
                $config['upload_path'] = './uploads/slider';
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['max_size'] = '10000';
                $ext = end(explode(".", $_FILES['userfile']['name']));
                $config['file_name'] = $this->set_img_nf($ext);
                //echo $ext; die;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    $this->render('admin/editslider', '', "", "Редактировать", "Введите пожалуйста данные слайда", array('error' => $this->upload->display_errors()));
                } else {
                    if ($sliderInfo->thumb != '' && file_exists(FCPATH . 'uploads/slider/' . $sliderInfo->thumb)) {
                        unlink(FCPATH . 'uploads/slider/' . $sliderInfo->thumb);
                    }
                    $config1['image_library'] = 'gd2';
                    $config1['source_image'] = './uploads/categories/' . $config['file_name'];
                    $config1['create_thumb'] = FALSE;
                    $config1['maintain_ratio'] = TRUE;
                    $config1['width'] = 725;
                    $config1['height'] = 280;
                    $this->load->library('image_lib', $config1);
                    $this->image_lib->resize();
                }
            }
            $this->slider_model->editSlider($sliderId, $this->input->post('title'), $this->input->post('subtitle'), $this->input->post('description'), (isset($config)) ? $config['file_name'] : "");
            $this->render('admin/common/success', '', "", "Редактировать ", "", array('scsMsg' => 'Слайд удачно отредактирован'));
        }
    }

    public function producthierarchy(){

        $this->render('admin/producthierarchy', '', "", "Категории и продукты", "", array('hierarchy'=>$this->products_model->getWholehierarchy()) );
    }

    public function addsupercategory()
    {
        $this->form_validation->set_rules('title', 'Имя Категории', 'required');
        $this->form_validation->set_rules('slug', 'Слэг', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/addsupercategory', '', "", "Добавить Категорию", "Введите пожалуйста данные новой категории");
        } else {
            if (isset($_FILES['userfile']['name']) && !empty($_FILES['userfile']['tmp_name'])) {
                $config['upload_path'] = './uploads/supercategories';
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['max_size'] = '10000';
                $ext = end(explode(".", $_FILES['userfile']['name']));
                $config['file_name'] = $this->set_img_nf($ext);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    $this->render('admin/addsupercategory', '', "", "Добавить Категорию ", "Введите пожалуйста данные новой категории", array('error' => $this->upload->display_errors()));
                } else {
                    $config1['image_library'] = 'gd2';
                    $config1['source_image'] = './uploads/supercategories/' . $config['file_name'];
                    $config1['create_thumb'] = FALSE;
                    $config1['maintain_ratio'] = TRUE;
                    $config1['width'] = 700;
                    $config1['height'] = 300;
                    $this->load->library('image_lib', $config1);
                    $this->image_lib->resize();
                }
            }
            $this->categories_model->addSupercategory($this->input->post('title'),$this->input->post('slug'), $this->input->post('description'), (isset($config)) ? $config['file_name'] : "");
            $this->render('admin/common/success', '', "", "Добавить Категорию ", "", array('scsMsg' => 'Категория удачно добавлена'));
        }
    }

    public function editsupercategory($supercategoryId)
    {
        $this->form_validation->set_rules('title', 'Имя категории', 'required');
        $this->form_validation->set_rules('slug', 'Слэг', 'required');

        $supercategoryInfo = $this->categories_model->getSupercategoryById($supercategoryId);

        if ($this->form_validation->run() == FALSE) {

            $this->render('admin/editsupercategory', '', "", "Редактировать категорию", "Введите пожалуйста данные категории", array('supercategoryInfo' => $supercategoryInfo));
        } else {
            if (isset($_FILES['userfile']['name']) && !empty($_FILES['userfile']['tmp_name'])) {
                $config['upload_path'] = './uploads/supercategories';
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['max_size'] = '10000';
                $ext = end(explode(".", $_FILES['userfile']['name']));
                $config['file_name'] = $this->set_img_nf($ext);
                //echo $ext; die;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    $this->render('admin/editsupercategory', '', "", "Редактировать категорию", "Введите пожалуйста данные категории", array('error' => $this->upload->display_errors(),'supercategoryInfo' => $supercategoryInfo));
                } else {
                    if ($supercategoryInfo->thumb != '' && file_exists(FCPATH . 'uploads/slider/' . $supercategoryInfo->thumb)) {
                        unlink(FCPATH . 'uploads/slider/' . $supercategoryInfo->thumb);
                    }
                    $config1['image_library'] = 'gd2';
                    $config1['source_image'] = './uploads/editsupercategory/' . $config['file_name'];
                    $config1['create_thumb'] = FALSE;
                    $config1['maintain_ratio'] = TRUE;
                    $config1['width'] = 700;
                    $config1['height'] = 300;
                    $this->load->library('image_lib', $config1);
                    $this->image_lib->resize();
                }
            }
            $this->categories_model->editSupercategory($supercategoryId, $this->input->post('title'),$this->input->post('slug'), $this->input->post('description'), (isset($config)) ? $config['file_name'] : "");
            $this->render('admin/common/success', '', "", "Редактировать категорию", "", array('scsMsg' => 'Категория удачно отредактирована'));
        }
    }


    public function addcategorysimple($superCategoryId)
    {
        $this->form_validation->set_rules('category_name', 'Имя подкатегории', 'required');
        $this->form_validation->set_rules('slug_name', 'слэг', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/addcategorysimple', '', "", "Добавить подкатегорию с диаметрамы и материаламы", "Введите пожалуйста данные новой категории",array('superCategoryId'=>$superCategoryId));
        } else {
            if (isset($_FILES['userfile']['name']) && !empty($_FILES['userfile']['tmp_name'])) {
                $config['upload_path'] = './uploads/categories';
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['max_size'] = '6048';
                $ext = end(explode(".", $_FILES['userfile']['name']));
                $config['file_name'] = $this->set_img_nf($ext);
                //echo $ext; die;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    $this->render('admin/addcategorysimple', '', "", "Добавить подкатегорию с диаметрамы и материаламы", "Введите пожалуйста данные новой категории", array('error' => $this->upload->display_errors(),'superCategoryId'=>$superCategoryId));
                } else {
                    $config1['image_library'] = 'gd2';
                    $config1['source_image'] = './uploads/categories/' . $config['file_name'];
                    $config1['create_thumb'] = FALSE;
                    $config1['maintain_ratio'] = TRUE;
                    $config1['width'] = 267;
                    $config1['height'] = 267;
                    $this->load->library('image_lib', $config1);
                    $this->image_lib->resize();
                }
            }
            $this->categories_model->addCategorySimple($this->input->post('category_name'), $this->input->post('slug_name'), $this->input->post('description'), (isset($config)) ? $config['file_name'] : "",$superCategoryId);
            $this->render('admin/common/success', '', "", "Добавить подкатегорию с диаметрамы и материаламы", "", array('scsMsg' => 'Подкатегория удачно добавлена'));
        }
    }

    public function editcategorysimple($categoryId)
    {
        $this->form_validation->set_rules('category_name', 'Имя категории', 'required');
        $this->form_validation->set_rules('slug_name', 'слэг', 'required');
        $categoryInfo = $this->categories_model->getCategorySimple($categoryId);
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/editcategorysimple', '', "", "Редактировать категорию", "Введите пожалуйста данные категории", array('categoryInfo' => $categoryInfo));
        } else {
            if (isset($_FILES['userfile']['name']) && !empty($_FILES['userfile']['tmp_name'])) {
                $config['upload_path'] = './uploads/categories';
                $config['allowed_types'] = 'jpg|jpeg|gif|png';
                $config['max_size'] = '6048';
                $ext = end(explode(".", $_FILES['userfile']['name']));
                $config['file_name'] = $this->set_img_nf($ext);
                //echo $ext; die;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    $this->render('admin/editcategorysimple', '', "", "Редактировать категорию", "Введите пожалуйста данные категории", array('error' => $this->upload->display_errors()));
                } else {
                    if ($categoryInfo['thumb'] != '' && file_exists(FCPATH . 'uploads/categories/' . $categoryInfo['thumb'])) {
                        unlink(FCPATH . 'uploads/categories/' . $categoryInfo['thumb']);
                    }
                    $config1['image_library'] = 'gd2';
                    $config1['source_image'] = './uploads/categories/' . $config['file_name'];
                    $config1['create_thumb'] = FALSE;
                    $config1['maintain_ratio'] = TRUE;
                    $config1['width'] = 267;
                    $config1['height'] = 267;
                    $this->load->library('image_lib', $config1);
                    $this->image_lib->resize();
                }
            }

            $this->categories_model->editCategorySimple($categoryId, $this->input->post('category_name'), $this->input->post('slug_name'), $this->input->post('description'), (isset($config)) ? $config['file_name'] : "");
            $this->render('admin/common/success', '', "", "Редактировать категорию", "", array('scsMsg' => 'Категория удачно отредактирована'));
        }
    }


    public function addproductsimple($category_id){
        $categoryInfo = $this->categories_model->getCategorySimple($category_id);

        $this->form_validation->set_rules('slugname', 'слэг', 'required');
        $this->form_validation->set_rules('price', 'Цена Продукта', 'required|is_natural_no_zero');

        $this->form_validation->set_rules('product_name', 'Имя Продукта', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/addproductsimple', '', "", "Добавить продукт к категории - ".$categoryInfo['title'], "Введите пожалуйста данные нового продукта", array('categoryInfo'=>$categoryInfo) );
        }
        else{
            if (isset($_POST['submit']))
            {


                $this->load->library('upload');


                foreach($_FILES as $field => $file)
                {

                    if ($field == "userfile"  && !empty($file['tmp_name'])){
                        $config=array();
                        $config['upload_path'] = './uploads/products/thumb';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['max_size'] = '6048';
                        $ext = end(explode(".", $file['name']));
                        $config['file_name'] = $this->set_img_nf($ext);
                        $this->upload->initialize($config);
                        if($file['error'] == 0)
                        {
                            if ($this->upload->do_upload($field))
                            {
                                $config_img['image_library'] = 'gd2';
                                $config_img['source_image'] = './uploads/products/thumb/' . $config['file_name'];
                                $config_img['create_thumb'] = FALSE;
                                $config_img['maintain_ratio'] = TRUE;
                                $config_img['width'] = 204;
                                $config_img['height'] = 204;
                                $this->load->library('image_lib', $config_img);
                                $this->image_lib->resize();
                            }
                            else
                            {
                                $this->render('admin/addproductsimple', '', "", "Добавить продукт к категории - ".$categoryInfo['title'], "Введите пожалуйста данные нового продукта", array('categoryInfo'=>$categoryInfo,'userfileerror'=>$this->upload->display_errors()) );
                            }
                        }
                    }
                    if ($field == "userfile1" && !empty($file['tmp_name'])){
                        $config1=array();
                        $config1['upload_path'] = './uploads/products/popup';
                        $config1['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config1['max_size'] = '6048';
                        $ext = end(explode(".", $file['name']));
                        $config1['file_name'] = $this->set_img_nf($ext);
                        $this->upload->initialize($config1);

                        if($file['error'] == 0)
                        {
                            // So lets upload
                            if ($this->upload->do_upload($field))
                            {
                                $data = $this->upload->data();
                            }
                            else
                            {
                                $categoryInfo = $this->categories_model->getCategory($category_id);
                                $this->render('admin/addproductsimple', '', "", "Добавить продукт к категории - ".$categoryInfo['title'], "Введите пожалуйста данные нового продукта", array('categoryInfo'=>$categoryInfo,'userfile1error'=>$this->upload->display_errors()) );
                            }
                        }
                    }
                    // No problems with the file

                }


            }
            $this->products_model->addProductSimple($category_id,$this->input->post('product_name'),$this->input->post('price'),$this->input->post('slugname'),(isset($config))?$config['file_name']:"",(isset($config1))?$config1['file_name']:"");
            $this->render('admin/common/success', '', "", "Добавить продукт", "", array('scsMsg' => 'Продукт удачно добавлен'));
        }
    }


    public function editproductsimple($product_id){
        $productInfo = $this->products_model->getProductSimpleByID($product_id);

        $this->form_validation->set_rules('slugname', 'слэг', 'required');

        $this->form_validation->set_rules('product_name', 'Имя Продукта', 'required');
        $this->form_validation->set_rules('price', 'Цена Продукта', 'required|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE) {
            $this->render('admin/editproductsimple', '', "", "Редактировать продукт", "Введите пожалуйста данные  продукта", array('productInfo'=>$productInfo) );
        }
        else{

            if (isset($_POST['submit']))
            {
              //  print_r($_POST); die;
                $this->load->library('upload');


                foreach($_FILES as $field => $file)
                {

                    if ($field == "userfile" && !empty($file['tmp_name'])){
                        $config=array();
                        $config['upload_path'] = './uploads/products/thumb';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['max_size'] = '6048';
                        $ext = end(explode(".", $file['name']));
                        $config['file_name'] = $this->set_img_nf($ext);
                        $this->upload->initialize($config);
                        if($file['error'] == 0)
                        {
                            if ($this->upload->do_upload($field))
                            {
                                if ($productInfo['product']->img_main != '' && file_exists(FCPATH . 'uploads/products/thumb/' . $productInfo['product']->img_main)) {
                                    unlink(FCPATH . 'uploads/products/thumb/' . $productInfo['product']->img_main);
                                }

                                $config_img['image_library'] = 'gd2';
                                $config_img['source_image'] = './uploads/products/thumb/' . $config['file_name'];
                                $config_img['create_thumb'] = FALSE;
                                $config_img['maintain_ratio'] = TRUE;
                                $config_img['width'] = 204;
                                $config_img['height'] = 204;
                                $this->load->library('image_lib', $config_img);
                                $this->image_lib->resize();
                            }
                            else
                            {

                                $this->render('admin/editproductsimple', '', "", "Редактировать продукт", "Введите пожалуйста данные  продукта", array('productInfo'=>$productInfo, 'userfileerror'=>$this->upload->display_errors()) );
                            }
                        }
                    }
                    if ($field == "userfile1" && !empty($file['tmp_name'])){
                        $config1=array();
                        $config1['upload_path'] = './uploads/products/popup';
                        $config1['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config1['max_size'] = '6048';
                        $ext = end(explode(".", $file['name']));
                        $config1['file_name'] = $this->set_img_nf($ext);
                        $this->upload->initialize($config1);

                        if($file['error'] == 0)
                        {
                            // So lets upload
                            if ($this->upload->do_upload($field))
                            {
                                if ($productInfo['product']->img_more != '' && file_exists(FCPATH . 'uploads/products/thumb/' . $productInfo['product']->img_more)) {
                                    unlink(FCPATH . 'uploads/products/popup/' . $productInfo['product']->img_more);
                                }
                            }
                            else
                            {
                                $this->render('admin/editproductsimple', '', "", "Редактировать продукт", "Введите пожалуйста данные  продукта", array('productInfo'=>$productInfo, 'userfileerror1'=>$this->upload->display_errors()) );
                            }
                        }
                    }
                    // No problems with the file

                }


            }
            $this->products_model->editProductSimple($product_id,$this->input->post('product_name'),$this->input->post('price'),$this->input->post('slugname'),(isset($config))?$config['file_name']:"",(isset($config1))?$config1['file_name']:"");
            $this->render('admin/common/success', '', "", "Редактировать продукт", "", array('scsMsg' => 'Продукт удачно отредактирован'));
        }
    }

    public function deleteproductsimple()
    {
        $this->products_model->deleteProductSimple($this->input->post('product_id'));
        exit;
    }

    public function deletesupercategory()
    {
        $this->categories_model->deleteSupercategory($this->input->post('supercategory_id'));
        exit;
    }

}