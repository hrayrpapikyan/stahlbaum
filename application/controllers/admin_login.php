<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_login extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('admin_user_model');

    }
    public function index()
    {
        $this->form_validation->set_rules('username', 'Username', 'required|callback_userExists');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('admin/common/header',array('title'=>$this->config->item('project_name')));
            $this->load->view('admin/login');
            $this->load->view('admin/common/footer');
        }
        else
        {
            $this->admin_user_model->setLogedIn();
            redirect('admin');
        }

    }

    function logout(){
        $this->admin_user_model->unsetLogedIn();
        redirect('admin_login');
    }

    public function userExists($username)
    {
        if (!$this->admin_user_model->userExists($username,$this->input->post('password')))
        {
            $this->form_validation->set_message('userExists', 'Введите правильные имя пользователя и пароль');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

}
