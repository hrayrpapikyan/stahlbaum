<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller
{
    function __construct()
    {


        parent::__construct();
        $this->load->model('admin_user_model');

        $this->load->model('options_model');
        $this->load->model('paymentandshipping_model');

        $this->load->model('categories_model');
        $this->load->model('products_model');
        $this->load->model('staticpages_model');
        $this->load->model('slider_model');
        $this->load->model('cart_model');
    }

    private function render($view,$data=array()){
        $options = $this->options_model->getOptions();
        //$this->categories_model->getAllCategoriesWithFirstDiameter()
        $this->load->view('front/common/header',array('options'=>$options));
        $this->load->view('front/common/header-top',array('options'=>$options));
        $this->load->view('front/common/slider-logo-menu',array('slider'=>$this->slider_model->getAll()));
        $this->load->view('front/common/left-menu',array('categories'=>$this->categories_model->getAllSupercategories()));
        $this->load->view($view,$data);
        $this->load->view('front/common/footer',array('options'=>$options));
    }

    public function index()
    {

        $this->render('front/home',array('superCategoryInfo'=>$this->categories_model->getAllSupercategories()));
    }

    public function category($id,$diameter){

        $this->render('front/category',array('categoryInfo'=>$this->categories_model->getCategory($id),
            'productsInfo'=>$this->products_model->getProductsByCategoryIdAndDiameter($id,$diameter),'chosenDiam'=>$diameter));
    }

    public function category_only_diameters($id){
        $this->render('front/categorynew',array('categoryInfo'=>$this->categories_model->getCategoryNew($id),
            'productsInfo'=>$this->products_model->getProductsNewByCategoryId($id)));
    }

    public function o_nas(){
        $this->render('front/aboutus',array('pageInfo'=>$this->staticpages_model->getPageContent('aboutus')));
    }

    public function pokupatelyam(){
        $this->render('front/tocustomers',array('pageInfo'=>$this->staticpages_model->getPageContent('tocustomers')));
    }

    public function dostavka_i_oplata(){
        $this->render('front/paymentdetails',array('pageInfo'=>$this->staticpages_model->getPageContent('shipping'),'options'=>$this->options_model->getOptions()));
    }

    public function kontakti(){
        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Ваша электронная почта', 'required|valid_email');
        $this->form_validation->set_rules('message', 'Написать сообщение', 'required');

        $opts = $this->options_model->getOptions();
        if ($this->form_validation->run() == FALSE)
        {
            $this->form_validation->set_error_delimiters('<p style="color:red">', '</p>');
        $this->render('front/contacts',array('options'=>$opts));
        }
        else{
            
            $config = [
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.yandex.com',
                'smtp_port' => 465,
                'smtp_user' => 'server@stahlbaum.ru',
                'smtp_pass' => 'stahlbaum',
                'mailtype'  => 'html', 
                'charset'   => 'utf-8'
            ];
            $this->load->library('email', $config);
            $this->email->from($this->config->item('server_email'), '');
            $this->email->to($opts->contacts_email);

            $this->email->subject('Писмо от посетителя: '.$this->input->post('email'));
            $this->email->message($this->input->post('message'));
            if($this->email->send()){
                //Success email Sent
                //echo $this->email->print_debugger();
            }else{

                //Email Failed To Send
                //echo $this->email->print_debugger();
            }
            $this->render('front/contacts',array('options'=>$opts,'success'=>'Спасибо, ваше письмо отправлено'));
        }
    }
	public function korzina_1(){
	    $this->render('front/cart1');
	}

    public function korzina_2(){
        if(!isset($_POST['submit'])){
        $this->render('front/cart2');
        }
        else{
        $aa =  $this->cart_model->sendOrder(
                $this->input->post('name'),
                $this->input->post('address1'),
                $this->input->post('city'),
                $this->input->post('tel'),
                $this->input->post('email'),
                $this->input->post('note'),
                $this->input->post('hdn'),
                $this->options_model->getOptions()
            );
           $this->render('front/cart3');
            //$this->load->view('front/test',array('pageInfo'=>$aa));
        }
    }

    public function supercategory($id){
        $this->render('front/catalog',array('categoryInfo'=>$this->categories_model->getCategoriesWithDiametersBySupercategoryId($id),'categoryInfoNew'=>$this->categories_model->getCategoriesNewBySupercategoryId($id)));
    }
    public function category_simple($id){

        $this->render('front/categorysimple',array('productsInfo'=>$this->products_model->getProductsSimpleByCategoryId($id)));
    }

    public function cartinformation(){
       echo json_encode($this->cart_model->getCart($this->input->post('data')));
        exit;
    }

    public function development(){
        $this->render('front/development');
    }

    public function poisk(){
        $searchWords = $this->input->post('searchwords');
//        echo $searchWords; die;
        $data =array();
        $data['linksToSuperCategories']=$this->categories_model->getSuperCategoriesLike($searchWords);
        $data['linksToCategories']=$this->categories_model->getCategoriesLike($searchWords);
        $data['linksToCategoriesNew']=$this->categories_model->getCategoriesNewLike($searchWords);
        $data['linksToProducts']=$this->products_model->getProductsLike($searchWords);
        $data['linksToProductsNew']=$this->products_model->getProductsNewLike($searchWords);
        $data['isNotEmpty']=count($data['linksToSuperCategories'])+count($data['linksToCategories'])+count($data['linksToCategoriesNew'])+count($data['linksToProducts'])+count($data['linksToProductsNew']);
        $this->render('front/searchresults',$data);

    }


}