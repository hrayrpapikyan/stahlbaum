<?php
class Staticpages_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    public function getPageContent($dbkey){
        return $this->db->where('dbkey',$dbkey)->get('pages')->row()->content;
    }
    public function editPageContent($dbkey,$pageContent){
        $this->db->where('dbkey',$dbkey)->update('pages',array('content'=>$pageContent));
    }

}