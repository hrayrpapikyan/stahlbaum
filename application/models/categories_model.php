<?php
class Categories_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
    public function addCategory($category_name,$slug_name,$description,$file_name,$diameters,$superCategoryId){
        $this->
            db->insert(
            'categories',
            array(
                'title'=>$category_name,
                'slug'=>$slug_name,
                'description'=>$description,
                'thumb'=>$file_name,
                'supercategory_id'=>$superCategoryId
            )
        );
        $catId=$this->db->insert_id();
        $diameters=str_replace(' ', '', $diameters);
        $diameters=explode(',',$diameters);
        foreach($diameters as $diameter){

            $this->db->insert('categories_diameters',array(
                'category_id' => $catId,
                'diameter' => $diameter
            ));

        }
    }
    public function getAllCategories(){
        return $this->db->get('categories')->result();
    }

    public function getAllCategoriesWithFirstDiameter(){
        $categories = $this->db->get('categories')->result();
        $data = array();
        foreach ($categories as $cat){
          array_push($data,array('category'=>$cat,'first_diam'=>$this->db->where('category_id',$cat->id)
              ->get('categories_diameters')->row()->diameter));
        }
        return $data;
    }
    public function getCategory($categoryId){

        $result = $this->db->where('id',$categoryId)->get('categories')->row();
        $data['title']=$result->title;
        $data['slug']=$result->slug;
        $data['description']=$result->description;
        $data['thumb']=$result->thumb;
        $data['id']=$result->id;


        $data['diameters']=$this->db->where('category_id',$categoryId)->order_by('diameter +0')->get('categories_diameters')->result();
     return $data;

    }


    public function getCategoryNew($categoryId){

        $result = $this->db->where('id',$categoryId)->get('categoriesnew')->row();
        $data['title']=$result->title;
        $data['slug']=$result->slug;
        $data['description']=$result->description;
        $data['thumb']=$result->thumb;
        $data['id']=$result->id;


        $data['diameters']=$this->db->where('category_id',$categoryId)->get('categories_diametersnew')->result();
        return $data;

    }
    public function editCategory($categoryID,$category_name,$slug_name,$description,$file_name,$diameters){
        if($file_name == ''){
            $updateArray=array('title'=>$category_name,
                'slug'=>$slug_name,
                'description'=>$description
            );
        }
        else{
            $updateArray=array('title'=>$category_name,
                'slug'=>$slug_name,
                'description'=>$description,
                'thumb'=>$file_name
            );
        }
        $this->db->where('id',$categoryID)
            ->update('categories',$updateArray);
if(isset($diameters) && !empty($diameters))
        foreach($diameters as $dms){
            $this->db->insert('categories_diameters',array('category_id'=>$categoryID,'diameter'=>$dms));
        }


    }


    public function editCategoryNew($categoryID,$category_name,$slug_name,$description,$file_name,$diameters){
        if($file_name == ''){
            $updateArray=array('title'=>$category_name,
                'slug'=>$slug_name,
                'description'=>$description
            );
        }
        else{
            $updateArray=array('title'=>$category_name,
                'slug'=>$slug_name,
                'description'=>$description,
                'thumb'=>$file_name
            );
        }
        $this->db->where('id',$categoryID)
            ->update('categoriesnew',$updateArray);
        if($diameters)
        foreach($diameters as $dms){
            $this->db->insert('categories_diametersnew',array('category_id'=>$categoryID,'diameter'=>$dms));
        }


    }


    public function deleteCategory($cid){
        $thumb= $this->db->where('id',$cid)->get('categories')->row()->thumb;
        $this->db->where('id',$cid)->delete('categories');
        $this->db->where('category_id',$cid)->delete('categories_diameters');
        if($thumb!="" && file_exists('./uploads/categories/'.$thumb)){
            unlink(FCPATH.'uploads/categories/'.$thumb);
        }

        $CI =& get_instance();
        //deleting products
        $products = $this->db->where('category_id',$cid)->get('products')->result();

        foreach($products as $product){
            $CI->load->model('products_model');
            $CI->products_model->deleteProduct($product->id);
        }


        //deleting diameters relation
        $this->db->where('category_id',$cid)->delete('categories_diameters');
    }


    public function deleteCategoryNew($cid){
        $thumb= $this->db->where('id',$cid)->get('categoriesnew')->row()->thumb;
        $this->db->where('id',$cid)->delete('categoriesnew');
        $this->db->where('category_id',$cid)->delete('categories_diametersnew');
        if($thumb!="" && file_exists('./uploads/categories/'.$thumb)){
            unlink(FCPATH.'uploads/categories/'.$thumb);
        }

        $CI =& get_instance();
        //deleting products
        $products = $this->db->where('category_id',$cid)->get('productsnew')->result();

        foreach($products as $product){
            $CI->load->model('products_model');
            $CI->products_model->deleteProductNew($product->id);
        }


        //deleting diameters relation
        $this->db->where('category_id',$cid)->delete('categories_diametersnew');
    }


    public function deleteCategorySimple($cid){
        $thumb= $this->db->where('id',$cid)->get('categories_simple')->row()->thumb;
        $this->db->where('id',$cid)->delete('categories_simple');
        if($thumb!="" && file_exists('./uploads/categories/'.$thumb)){
            unlink(FCPATH.'uploads/categories/'.$thumb);
        }

        $CI =& get_instance();
        //deleting products
        $products = $this->db->where('category_id',$cid)->get('products_simple')->result();

        foreach($products as $product){
            $CI->load->model('products_model');
            $CI->products_model->deleteproductsimple($product->id);
        }
    }

    public function deleterelation($relation_id, $diameter){
        $catID = $this->db->where('id',$relation_id)->get('categories_diameters')->row()->category_id;
        $products = $this->db->where('category_id',$catID)->get('products')->result();
        foreach($products as $product){
            $this->db->where('product_id',$product->id)->where('diameter',$diameter)->delete('products_materials_relation');
        }

        $this->db->where('id',$relation_id)->delete('categories_diameters');
    }

    public function deleterelationnew($relation_id, $diameter){
        $catID = $this->db->where('id',$relation_id)->get('categories_diametersnew')->row()->category_id;
        $products = $this->db->where('category_id',$catID)->get('productsnew')->result();
        foreach($products as $product){
            $this->db->where('product_id',$product->id)->where('diameter',$diameter)->delete('products_diameters_relation');
        }

        $this->db->where('id',$relation_id)->delete('categories_diametersnew');
    }

    public function getCategoriesWithDiametersBySupercategoryId($supercategoryId){
        $categories = $this->db->where('supercategory_id',$supercategoryId)->get('categories')->result();
        $data = array();
        foreach ($categories as $category)
        {
            array_push($data,array('category'=>$category,'diameters'=>$this->db->where('category_id',$category->id)->order_by('diameter +0')->get('categories_diameters')->result()));
        }
        return $data;
    }

    public function addSupercategory($title,$slug,$description,$file_name){
        $this->
            db->insert(
            'supercategories',
            array(
                'title'=>$title,
                'description'=>$description,
                'thumb'=>$file_name,
                'slug'=>$slug
            )
        );
    }


public function getSupercategoryById($supercategoryId){
   return $this->db->where('id',$supercategoryId)->get('supercategories')->row();
}


    public function editSupercategory($supercategoryId, $title, $slug, $description, $file_name){
        if($file_name == ''){
            $updateArray=array(
                'title'=>$title,
                'description'=>$description,
                'slug'=>$slug
            );
        }
        else{
            $updateArray=array(
                'title'=>$title,
                'description'=>$description,
                'slug'=>$slug,
                'thumb'=>$file_name
            );
        }
        $this->db->where('id',$supercategoryId)
            ->update('supercategories',$updateArray);
    }


    public function addCategorySimple($category_name,$slug_name,$description,$file_name,$superCategoryId){
        $this->
            db->insert(
            'categories_simple',
            array(
                'title'=>$category_name,
                'slug'=>$slug_name,
                'description'=>$description,
                'thumb'=>$file_name,
                'supercategory_id'=>$superCategoryId
            )
        );

    }

    public function getCategorySimple($categoryId){

        $result = $this->db->where('id',$categoryId)->get('categories_simple')->row();
        $data = array();
        $data['title']=$result->title;
        $data['slug']=$result->slug;
        $data['description']=$result->description;
        $data['thumb']=$result->thumb;
        $data['id']=$result->id;

     return $data;
    }

    public function editCategorySimple($categoryID,$category_name,$slug_name,$description,$file_name){
        if($file_name == ''){
            $updateArray=array('title'=>$category_name,
                'slug'=>$slug_name,
                'description'=>$description
            );
        }
        else{
            $updateArray=array('title'=>$category_name,
                'slug'=>$slug_name,
                'description'=>$description,
                'thumb'=>$file_name
            );
        }
        $this->db->where('id',$categoryID)
            ->update('categories_simple',$updateArray);


    }
  public function deleteSupercategory($sid){
          $thumb= $this->db->where('id',$sid)->get('supercategories')->row()->thumb;
          $this->db->where('id',$sid)->delete('supercategories');
          if($thumb!="" && file_exists('./uploads/supercategories/'.$thumb)){
              unlink(FCPATH.'uploads/supercategories/'.$thumb);
          }

          //deleting categories
      $categories = $this->db->where('supercategory_id',$sid)->get('categories')->result();
      foreach($categories as $cat){
          $this->deleteCategory($cat->id);
      }

      //deleting simple categories
      $categories = $this->db->where('supercategory_id',$sid)->get('categories_simple')->result();
      foreach($categories as $cat){
          $this->deleteCategorySimple($cat->id);
      }

  }

    public function getAllSupercategories(){
        return $this->db->get('supercategories')->result();
    }

    public function getCategoriesSimpleBySupercategoryId($sid){
        return $this->db->where('supercategory_id',$sid)->get('categories_simple')->result();
    }

    public function getCategoriesNewBySupercategoryId($sid){
        return $this->db->where('supercategory_id',$sid)->get('categoriesnew')->result();
    }

    public function addCategoryNew($category_name,$slug_name,$description,$file_name,$diameters,$superCategoryId){
        $this->
            db->insert(
            'categoriesnew',
            array(
                'title'=>$category_name,
                'slug'=>$slug_name,
                'description'=>$description,
                'thumb'=>$file_name,
                'supercategory_id'=>$superCategoryId
            )
        );
        $catId=$this->db->insert_id();
        $diameters=str_replace(' ', '', $diameters);
        $diameters=explode(',',$diameters);
        foreach($diameters as $diameter){

            $this->db->insert('categories_diametersnew',array(
                'category_id' => $catId,
                'diameter' => $diameter
            ));

        }
    }
    public function getSuperCategoriesLike($search){
        return $this->db->like('title',$search)->get('supercategories')->result();
    }

    public function getCategoriesLike($search){
        $categories = $this->db->like('title',$search)->get('categories')->result();
        $data = array();
        foreach ($categories as $category)
        {
            array_push($data,array('category'=>$category,'diameters'=>$this->db->where('category_id',$category->id)->get('categories_diameters')->result()));
        }
        return $data;
    }

    public function getCategoriesNewLike($search){
        return $this->db->like('title',$search)->get('categoriesnew')->result();
    }
}