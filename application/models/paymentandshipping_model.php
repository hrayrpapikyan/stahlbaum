<?php
class Paymentandshipping_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }



    public function updateShipping($title,$price,$description){
        for($i=1;$i<=count($title);$i++){
            $this->db->where('id',$i)
                ->update('shipping',
                array(
                    'title'=>$title[$i-1],
                    'description'=>$description[$i-1],
                    'price'=>($price[$i-1]=="")?0:$price[$i-1]
                ));
        }
    }

    public function getShippingData(){
        return $this->db->get('shipping')->result();
    }

    public function updatePayment($title,$price,$description){
        for($i=1;$i<=count($title);$i++){
            $this->db->where('id',$i)
                ->update('payment',
                array(
                    'title'=>$title[$i-1],
                    'description'=>$description[$i-1],
                    'price'=>($price[$i-1]=="")?0:$price[$i-1]
                ));
        }
    }

    public function getPaymentData(){
        return $this->db->get('payment')->result();
    }

}