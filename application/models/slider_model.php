<?php
class Slider_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    public function addslider($title,$subtitle,$description,$file_name){
        $this->
            db->insert(
            'slider',
            array(
                'title'=>$title,
                'subtitle'=>$subtitle,
                'description'=>$description,
                'thumb'=>$file_name
            )
        );
    }

    public function getAll(){
        return $this->db->get('slider')->result();
    }

    public function deleteslider($pid){
        $thumb= $this->db->where('id',$pid)->get('slider')->row()->thumb;

        $this->db->where('id',$pid)->delete('slider');

        if($thumb!="" && file_exists(FCPATH.'/uploads/slider/'.$thumb)){
            unlink(FCPATH.'/uploads/slider/'.$thumb);
        }

    }

    public function getsliderById($sliderId){
       return $this->db->where('id',$sliderId)->get('slider')->row();
    }

    public function editSlider($sliderId, $title, $subtitle, $description, $file_name){
        if($file_name == ''){
            $updateArray=array(
                'title'=>$title,
                'description'=>$description,
                'subtitle'=>$subtitle
            );
        }
        else{
            $updateArray=array(
                'title'=>$title,
                'description'=>$description,
                'subtitle'=>$subtitle,
                'thumb'=>$file_name
            );
        }
        $this->db->where('id',$sliderId)
            ->update('slider',$updateArray);
}

}