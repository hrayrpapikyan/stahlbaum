<?php
class Admin_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    public function getPageContent($dbkey){
        return $this->db->where('dbkey',$dbkey)->get('pages')->row()->content;
    }
    public function editPageContent($dbkey,$pageContent){
        $this->db->where('dbkey',$dbkey)->update('pages',array('content'=>$pageContent));
    }


    public function getCategoriesWithProducts(){
        $allCategories = $this->getAllCategories();
        $data = array();
        $i=0;
        foreach($allCategories as $category){
            $data[$i]['category_name']=$category->title;
            $data[$i]['category_id']=$category->id;
            $data[$i++]['product']=$this->db->where('category_id',$category->id)->get('products')->result();
        }
        return $data;
    }

    public function addProduct($category_id,$product_name,$slugname,$file_name,$file_name1,$diams,$mataerial_long,$material_short,$material_price){
    $this->db->insert('products',array(
        'category_id' => $category_id,
        'title' => $product_name,
        'slug' => $slugname,
        'img_main' => $file_name,
        'img_more' => $file_name1
    ));

        $product_id = $this->db->insert_id();


        foreach($diams as $diam){
          $this->db->insert('products_diameters', array(
              'product_id'=>$product_id,
              'diameter' => $diam
          ));
        }


        for ($i=0; $i<count($mataerial_long); $i++){
            $this->db->insert('materials',array(
                'long_description' =>  $mataerial_long[$i],
                'short_description' => $material_short[$i],
                'price' => $material_price[$i],
            ));

            $material_id=$this->db->insert_id();

            $this->db->insert('products_materials_relation', array(
                'product_id' => $product_id,
                'material_id' => $material_id
            ));
        }
    }

    public function getProductByID($product_id){
      $data=array();
        /*
           SELECT cd.diameter
           FROM categories_diameters AS cd
           INNER JOIN products AS p ON p.category_id = cd.category_id
           WHERE p.id = 1
        */

        $data['allDiametersForCategory'] = $this->db->select('cd.diameter')
            ->from('categories_diameters AS cd')
            ->join('products AS p','p.category_id = cd.category_id','INNER')
            ->where('p.id',$product_id)
            ->get()->result();
       /*
       SELECT pd.diameter
       FROM products_diameters AS pd
       INNER JOIN products AS p ON p.id = pd.product_id
       WHERE p.id = 2*/

        $availableDiametersForProductObj = $this->db->select('pd.diameter')
            ->from('products_diameters AS pd')
            ->join('products AS p','p.id = pd.product_id','INNER')
            ->where('p.id',$product_id)
            ->get()->result();

        $data['availableDiametersForProduct']=array();
        foreach($availableDiametersForProductObj as $item){
            array_push($data['availableDiametersForProduct'],$item->diameter);
        }

        /*
         SELECT m.*
         FROM materials AS m
          RIGHT JOIN products_materials_relation AS pmr ON pmr.material_id=m.id
         INNER JOIN products AS p ON p.id = pmr.product_id
         WHERE p.id = 2
         */

        $data['materialsForProduct'] = $this->db->select('m.*')
            ->from('materials AS m')
            ->join('products_materials_relation AS pmr','pmr.material_id=m.id','RIGHT')
            ->join('products AS p','p.id = pmr.product_id','INNER')
            ->where('p.id',$product_id)
            ->get()->result();

        $data['product'] = $this->db->where('id',$product_id)->get('products')->row();

        return $data;

    }

    public function editProduct($product_id,$product_name,$slugname,$file_name,$file_name1,$diams,$mataerial_long,$material_short,$material_price){

        $this->db->where('id',$product_id)
        ->update('products',array(
            'title' => $product_name,
            'slug' => $slugname,
        ));

        if($file_name!=''){
            $this->db->where('id',$product_id)
                ->update('products',array(
                'img_main' => $file_name,
            ));
        }

        if($file_name1!=''){
            $this->db->where('id',$product_id)
                ->update('products',array(
                'img_more' => $file_name1,
            ));
        }



        $this->db->where('product_id',$product_id)->delete('products_diameters');
        foreach($diams as $diam){
            $this->db->insert('products_diameters', array(
                'product_id'=>$product_id,
                'diameter' => $diam
            ));
        }

        $this->db->where('product_id',$product_id)->delete('products_materials_relation');
        for ($i=0; $i<count($mataerial_long); $i++){
            $this->db->insert('materials',array(
                'long_description' =>  $mataerial_long[$i],
                'short_description' => $material_short[$i],
                'price' => $material_price[$i],
            ));

            $material_id=$this->db->insert_id();

            $this->db->insert('products_materials_relation', array(
                'product_id' => $product_id,
                'material_id' => $material_id
            ));
        }
    }


    public function deleteProduct($pid){
        $thumb= $this->db->where('id',$pid)->get('products')->row()->img_main;
        $popup= $this->db->where('id',$pid)->get('products')->row()->img_more;
        $this->db->where('id',$pid)->delete('products');
        $this->db->where('product_id',$pid)->delete('products_diameters');
        $this->db->where('product_id',$pid)->delete('products_materials_relation');
        if($thumb!="" && file_exists(FCPATH.'/uploads/products/thumb/'.$thumb)){
            unlink(FCPATH.'/uploads/products/thumb/'.$thumb);
        }
        if($thumb!="" && file_exists(FCPATH.'/uploads/products/popup/'.$popup)){
            unlink(FCPATH.'/uploads/products/popup/'.$popup);
        }
    }


}