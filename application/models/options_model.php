<?php
class Options_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }


    public function getOptions(){
        return $this->db->get('admin_options')->row();
    }

    public function editOptions($tel1,$tel2,$address1,$address2,$contacts_email,$sales_email1,$map,$footer_text,$meta_keyw,$meta_desc,$title){
        $this->db->update('admin_options',
            array(
                'tel1'=>$tel1,
                'tel2'=>$tel2,
                'address1'=>$address1,
                'address2'=>$address2,
                'contacts_email'=>$contacts_email,
                'sales_email1'=>$sales_email1,
                'meta_keyw'=>$meta_keyw,
                'meta_desc'=>$meta_desc,
                'title'=>$title,

                'map'=>$map,
                'footer_text'=>$footer_text
            )
        );
    }

}