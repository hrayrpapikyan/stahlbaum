<?php
class Admin_user_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
    function userExists($username,$password)
    {
        $this->
            db->where('username',$username);
        $this->db->where('password',md5($password));
        return $this->db->get('admin')->num_rows();
    }
    function setLogedIn(){
        $this->session->set_userdata(array("logedIn"=>true));
    }
    function unsetLogedIn(){
        $this->session->unset_userdata(array("logedIn"=>true));
    }
    function isLogedIn(){
        return $this->session->userdata("logedIn");
    }
    function getUsername(){
        return $this->db->get('admin')->row()->username;
    }
}