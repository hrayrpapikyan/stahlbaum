<?php
class Products_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    public function getAllCategories(){
        return $this->db->get('categories')->result();
    }


    public function getCategoriesWithProducts(){
        $allCategories = $this->getAllCategories();
        $data = array();
        $i=0;
        foreach($allCategories as $category){
            $data[$i]['category_name']=$category->title;
            $data[$i]['category_id']=$category->id;
            $data[$i++]['product']=$this->db->where('category_id',$category->id)->get('products')->result();
        }
        return $data;
    }

    public function addProduct($category_id,$product_name,$slugname,$file_name,$file_name1,$mataerial_long,$material_short,$material_price){
        $this->db->insert('products',array(
            'category_id' => $category_id,
            'title' => $product_name,
            'slug' => $slugname,
            'img_main' => $file_name,
            'img_more' => $file_name1
        ));

        $product_id = $this->db->insert_id();

        $diametersPerMaterials = count($material_price)/count($mataerial_long);
        for ($i=0; $i<count($mataerial_long); $i++){

            $this->db->insert('materials',array(
                'long_description' =>  $mataerial_long[$i],
                'short_description' => $material_short[$i],
            ));

            $material_id=$this->db->insert_id();


            for ($j=($i*$diametersPerMaterials); $j<(($i+1)*$diametersPerMaterials); $j++){
                $this->db->insert('products_materials_relation', array(
                    'product_id' => $product_id,
                    'material_id' => $material_id,
                    'diameter' => $this->grabDiameter($material_price[$j]),
                    'price' => $this->grabPrice($material_price[$j])
                ));
            }


        }
    }

    public function addProductNew($category_id,$product_name,$slugname,$file_name,$file_name1,$diameter_price){
        $this->db->insert('productsnew',array(
            'category_id' => $category_id,
            'title' => $product_name,
            'slug' => $slugname,
            'img_main' => $file_name,
            'img_more' => $file_name1
        ));

        $product_id = $this->db->insert_id();


           foreach($diameter_price as $d_price)
                $this->db->insert('products_diameters_relation', array(
                    'product_id' => $product_id,
                    'diameter' => $this->grabDiameter($d_price),
                    'price' => $this->grabPrice($d_price)
                ));



        }



    private function grabDiameter($string){
       return substr($string, 0, strpos($string, '-'));
    }
    private function grabPrice($string){
        return substr($string, strpos($string, "-") + 1);
    }

    public function getProductByID($product_id){
        $data=array();


        $data['allDiametersForCategory'] = $this->db->select('cd.diameter')
            ->from('categories_diameters AS cd')
            ->join('products AS p','p.category_id = cd.category_id','INNER')
            ->where('p.id',$product_id)
            ->get()->result();


        $data['materialsForProduct'] = $this->db->select('m.*')
            ->from('materials AS m')
            ->join('products_materials_relation AS pmr','pmr.material_id=m.id','RIGHT')
            ->where('pmr.product_id',$product_id)
           ->group_by('id')
            ->get()->result();

         $data['materialsForProductWithDiametersAndPrices']=array();

         $data['product'] = $this->db->where('id',$product_id)->get('products')->row();

         $allDiameters = $this->db->where('category_id',$data['product']->category_id)->get('categories_diameters')->result();

            foreach($data['materialsForProduct'] as $aa){
                $data['materialsForProductWithDiametersAndPrices'][$aa->id] = array();
                foreach($allDiameters as $allD){

                $rs = $this->db->where('product_id',$product_id)->where('diameter',$allD->diameter)->where('material_id',$aa->id)->get('products_materials_relation');
                if($rs->num_rows()>0){
				$pr1 = $rs->result();
                    $pr = $pr1[0]->price;
                }
                 else{
                     $pr="";
                 }
                array_push($data['materialsForProductWithDiametersAndPrices'][$aa->id],array('diam'=>$allD->diameter, 'price'=>$pr)) ;
            }
        }

        return $data;

    }

    public function getProductByIDNew($product_id){
        $data=array();


        $data['allDiametersForCategory'] = $this->db->select('cd.diameter')
            ->from('categories_diametersnew AS cd')
            ->join('productsnew AS p','p.category_id = cd.category_id','INNER')
            ->where('p.id',$product_id)
            ->get()->result();


        $data['product'] = $this->db->where('id',$product_id)->get('productsnew')->row();

        $allDiameters = $this->db->where('category_id',$data['product']->category_id)->get('categories_diametersnew')->result();
$data['ProductWithDiametersAndPrices']=array();
            foreach($allDiameters as $allD){

                $rs = $this->db->where('product_id',$product_id)->where('diameter',$allD->diameter)->get('products_diameters_relation');
                if($rs->num_rows()>0){
                    $pr1 = $rs->result();
                    $pr = $pr1[0]->price;
                }
                else{
                    $pr="";
                }
                array_push($data['ProductWithDiametersAndPrices'],array('diam'=>$allD->diameter, 'price'=>$pr)) ;
            }

        return $data;

    }

    function getBodyForNewMaterialEdit($product_id){
        $this->db->insert('materials', array('long_description'=>''));
        $material_id=$this->db->insert_id();

        $materialHTML = '<div class="form-group" style="border:1px solid blue;border-radius:10px;padding:5px;">'.
        '<input type="hidden" name="mataerial_id_keeper[]" value="'.$material_id.'" class="form-control material-id-keeper">'.
        '<label>Длинное название</label>'  .
        '<input type="text" name="mataerial_long[]" value="" class="form-control">'   .
        '<label>Короткое название</label>'   .
        '<input type="text" name="material_short[]" value="" class="form-control">';

        $category_id = $this->db->where('id',$product_id)->get('products')->row()->category_id;
        $cats = $this->db->where('category_id',$category_id)->get('categories_diameters')->result();

        foreach($cats as $cat){
         $materialHTML .=   ' <label >Цена</label>'.
             '<input type="text" name="material_price[]" value="'.$cat->diameter.'-"'.' class="form-control">';
        }
        $materialHTML .= '<label class="remove_material_edit"><i class="fa fa-minus-circle fa-fw" style="color:RED"></i> Удалить</label></div>';
        return $materialHTML;
    }


    public function editProduct($product_id,$product_name,$slugname,$file_name,$file_name1,$mataerial_long,$material_short,$material_price,$mataerial_id_keeper){

        $this->db->where('id',$product_id)
            ->update('products',array(
            'title' => $product_name,
            'slug' => $slugname,
        ));

        if($file_name!=''){
            $this->db->where('id',$product_id)
                ->update('products',array(
                'img_main' => $file_name,
            ));
        }

        if($file_name1!=''){
            $this->db->where('id',$product_id)
                ->update('products',array(
                'img_more' => $file_name1,
            ));
        }



        for ($i=0; $i<count($mataerial_long); $i++){
            $this->db->where('id',$mataerial_id_keeper[$i])->update('materials',array(
                'long_description' => $mataerial_long[$i],
                'short_description' => $material_short[$i]
            ));
        }

        $diametersPerMaterials = count($material_price)/count($mataerial_long);
        for ($i=0; $i<count($mataerial_long); $i++){

            for ($j=($i*$diametersPerMaterials); $j<(($i+1)*$diametersPerMaterials); $j++){

                $rs = $this->db->where('material_id',$mataerial_id_keeper[$i])->where('product_id',$product_id)->where('diameter',$this->grabDiameter($material_price[$j]))->get('products_materials_relation');

                if($rs->num_rows()>0){
                    $this->db->where('material_id',$mataerial_id_keeper[$i])->where('product_id',$product_id)->where('diameter',$this->grabDiameter($material_price[$j]))->update('products_materials_relation',array('price'=>$this->grabPrice($material_price[$j])));
                }
                 else{
                $this->db->insert('products_materials_relation', array(
                    'product_id' => $product_id,
                    'material_id' => $mataerial_id_keeper[$i],
                    'diameter' => $this->grabDiameter($material_price[$j]),
                    'price' => $this->grabPrice($material_price[$j])
                ));
                 }

            }


        }

    }


    public function editProductNew($product_id,$product_name,$slugname,$file_name,$file_name1,$diameter_price){

        $this->db->where('id',$product_id)
            ->update('productsnew',array(
            'title' => $product_name,
            'slug' => $slugname,
        ));

        if($file_name!=''){
            $this->db->where('id',$product_id)
                ->update('productsnew',array(
                'img_main' => $file_name,
            ));
        }

        if($file_name1!=''){
            $this->db->where('id',$product_id)
                ->update('productsnew',array(
                'img_more' => $file_name1,
            ));
        }

            foreach ($diameter_price as $d_price){

                  $this->db->where('product_id',$product_id)->where('diameter',$this->grabDiameter($d_price))->update('products_diameters_relation',array('price'=>$this->grabPrice($d_price)));

            }




    }



    public function deleteProduct($pid){
        $thumb= $this->db->where('id',$pid)->get('products')->row()->img_main;
        $popup= $this->db->where('id',$pid)->get('products')->row()->img_more;
        $this->db->where('id',$pid)->delete('products');
        $this->db->where('product_id',$pid)->delete('products_materials_relation');
        if($thumb!="" && file_exists(FCPATH.'/uploads/products/thumb/'.$thumb)){
            unlink(FCPATH.'/uploads/products/thumb/'.$thumb);
        }
        if($thumb!="" && file_exists(FCPATH.'/uploads/products/popup/'.$popup)){
            unlink(FCPATH.'/uploads/products/popup/'.$popup);
        }

        $this->db->where('product_id',$pid)->delete('products_materials_relation');
    }


    public function deleteProductNew($pid){
        $thumb= $this->db->where('id',$pid)->get('productsnew')->row()->img_main;
        $popup= $this->db->where('id',$pid)->get('productsnew')->row()->img_more;
        $this->db->where('id',$pid)->delete('productsnew');
        $this->db->where('product_id',$pid)->delete('products_diameters_relation');
        if($thumb!="" && file_exists(FCPATH.'/uploads/products/thumb/'.$thumb)){
            unlink(FCPATH.'/uploads/products/thumb/'.$thumb);
        }
        if($thumb!="" && file_exists(FCPATH.'/uploads/products/popup/'.$popup)){
            unlink(FCPATH.'/uploads/products/popup/'.$popup);
        }
    }


    public function removematerialedit($product_id,$material_id){
       $this->db->where('product_id',$product_id)->where('material_id',$material_id)->delete('products_materials_relation');
    }

    public function getProductsByCategoryIdAndDiameter($categoryId,$diam){
        $data = array();
        $products = $this->db->where('category_id',$categoryId)->get('products')->result();
        foreach ($products as $product){

            $prc = $this->db->where('product_id',$product->id)
            ->where('diameter',str_replace('-','/',$diam))->get('products_materials_relation')->result();
            $prc_and_mat = array();
            foreach($prc as $pr){
             array_push($prc_and_mat,array('prc'=>$pr,'mat'=>$this->db->where('id',$pr->material_id)->get('materials')->row()));
            }
          array_push($data,array('product'=>$product,'pricesAndMaterials'=>$prc_and_mat));
        }

        return $data;
    }


    public function getProductsNewByCategoryId($categoryId){
        $data = array();
        $products = $this->db->where('category_id',$categoryId)->get('productsnew')->result();
        foreach ($products as $product){

            $prc = $this->db->where('product_id',$product->id)
                ->get('products_diameters_relation')->result();
            $prc_and_mat = array();
            foreach($prc as $pr){
                array_push($prc_and_mat,array('prc'=>$pr));
            }
            array_push($data,array('product'=>$product,'diametersAndMaterials'=>$prc_and_mat));
        }
        return $data;
    }

    public function getWholehierarchy(){
        $superCategories = $this->db->get('supercategories')->result();
        $ret=array();
        $i =0;
        foreach($superCategories as $superCategory){
            $ret[$i]['supercategory'] = $superCategory;
            $ret[$i]['categories'] = $this->db->where('supercategory_id',$superCategory->id)->get('categories')->result();
            $ret[$i]['products']=array();
            foreach($ret[$i]['categories'] as $catt){
               $ret[$i]['products'][$catt->id]=$this->db->where('category_id',$catt->id)->get('products')->result();
            }

            $ret[$i]['categoriesnew'] = $this->db->where('supercategory_id',$superCategory->id)->get('categoriesnew')->result();
            $ret[$i]['productsnew']=array();
            foreach($ret[$i]['categoriesnew'] as $catt){
                $ret[$i]['productsnew'][$catt->id]=$this->db->where('category_id',$catt->id)->get('productsnew')->result();
            }

             $i++;
        }
        return $ret;
    }

    public function addProductSimple($category_id,$product_name,$price,$slugname,$file_name,$file_name1){
        $this->db->insert('products_simple',array(
            'category_id' => $category_id,
            'title' => $product_name,
            'slug' => $slugname,
            'img_main' => $file_name,
            'img_more' => $file_name1,
            'price' => $price
        ));


    }


    public function editProductSimple($product_id,$product_name,$price,$slugname,$file_name,$file_name1){

        $this->db->where('id',$product_id)
            ->update('products_simple',array(
            'title' => $product_name,
            'slug' => $slugname,
            'price' => $price,
        ));

        if($file_name!=''){
            $this->db->where('id',$product_id)
                ->update('products_simple',array(
                'img_main' => $file_name,
            ));
        }

        if($file_name1!=''){
            $this->db->where('id',$product_id)
                ->update('products_simple',array(
                'img_more' => $file_name1,
            ));
        }


        }

    public function getProductSimpleByID($product_id){
        $data=array();
        $data['product'] = $this->db->where('id',$product_id)->get('products_simple')->row();
        return $data;

    }


    public function deleteproductsimple($cid){
        $img_main= $this->db->where('id',$cid)->get('products_simple')->row()->img_main;
        $img_more= $this->db->where('id',$cid)->get('products_simple')->row()->img_more;
        $this->db->where('id',$cid)->delete('products_simple');
        if($img_main!="" && file_exists('./uploads/products/thumb/'.$img_main)){
            unlink(FCPATH.'uploads/products/thumb/'.$img_main);
        }
        if($img_more!="" && file_exists('./uploads/products/popup/'.$img_more)){
            unlink(FCPATH.'uploads/products/popup/'.$img_more);
        }
    }


    public function getProductsSimpleByCategoryId($id){
        return $this->db->where('category_id',$id)->get('products_simple')->result();
    }

    public function getProductsLike($search){
        $data=array();
        $products = $this->db->like('title',$search)->get('products')->result();
        foreach($products as $product){
        $category_id = $product->category_id;
        $category = $this->db->where('id',$category_id)->get('categories')->row();
       $firstDiameter = $this->db->where('category_id',$category_id)->get('categories_diameters')->row()->diameter;
        $data[] = array(
            'products'=>$product,
            'category_id'=>$category_id,
            'firstDIameter'=>$firstDiameter,
            'category'=>$category
        );
        }
        return $data;
    }

    public function getProductsNewLike($search){
        $data=array();
        $products = $this->db->like('title',$search)->get('productsnew')->result();
        foreach($products as $product){
            $category_id = $product->category_id;
            $category = $this->db->where('id',$category_id)->get('categoriesnew')->row();
            $data[] = array(
                'products'=>$product,
                'category_id'=>$category_id,
                'category'=>$category
            );
        }

        return $data;
    }

}