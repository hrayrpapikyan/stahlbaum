<?php
class Cart_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    private function getProductById($prId){
        return $this->db->where('id',$prId)->get('products')->row();
    }

    private function getProductnewById($prId){
        return $this->db->where('id',$prId)->get('productsnew')->row();
    }

    private function getMaterialById($mId){
        return $this->db->where('id',$mId)->get('materials')->row();
    }
    private function getAllMaterialsByDiameterAndProductId($prId,$diameter){
        return $this->db->where('product_id',$prId)->where('diameter',$diameter)->get('products_materials_relation')->result();
    }

    private function getAllDiametersByProductId($prId){
        return $this->db->where('product_id',$prId)->get('products_diameters_relation')->result();
    }


   public function getCart($data){
       $data = json_decode($data,true);
       $ret=array();
       $ret['products']=array();
       $ret['productsSimple']=array();
       $ret['productsnew']=array();
       $ret['shipping']= array();
       $ret['payment']=array();
       $ret['productsTotal']=0;
       foreach($data['products'] as $product){
           $allMaterialsRelation=$this->getAllMaterialsByDiameterAndProductId($product['product_id'],$product['diameter']);
          // print_r($allMaterialsRelation); die;
           $allMaterialsWithDetails=array();
           //collecting all materials for product, marking which one is selected
           foreach($allMaterialsRelation as $it){
               $oneMaterial = $this->getMaterialById($it->material_id);
               $oneMaterialWithDetails = array(
                   'isChosen'=>($it->material_id == $product['material_id'])?true:false,
                   'long_description'=>$oneMaterial->long_description,
                   'short_description'=>$oneMaterial->short_description,
                   'price'=>$it->price,
                   'id'=>$it->material_id
               );
               array_push($allMaterialsWithDetails,$oneMaterialWithDetails);
           }

           $productDetails = $this->getProductById($product['product_id']);
           $quantity=$product['quantity'];
           $pricePerItem = $product['pricePerItem'];
           $priceTotal = $product['priceTotal'];
           array_push($ret['products'],array(
               'allMaterialsWithDetails'=>$allMaterialsWithDetails,
               'productDetails'=>$productDetails,
               'pricePerItem'=>$pricePerItem,
               'priceTotal'=>$priceTotal,
               'quantity'=>$quantity,
               'diameter'=>$product['diameter']
           ));
           $ret['productsTotal']+=$priceTotal;
       }



       foreach($data['productsnew'] as $product){
           $allMaterialsRelation=$this->getAllDiametersByProductId($product['product_id']);
           $allMaterialsWithDetails=array();
           //collecting all materials for product, marking which one is selected
           foreach($allMaterialsRelation as $it){
               $oneMaterialWithDetails = array(
                   'diameter'=>$it->diameter,
                   'isChosen'=>($it->diameter == $product['diameter'])?true:false,
                   'price'=>$it->price,
               );
               array_push($allMaterialsWithDetails,$oneMaterialWithDetails);
           }
           $productDetails = $this->getProductnewById($product['product_id']);
           $quantity=$product['quantity'];
           $pricePerItem = $product['pricePerItem'];
           $priceTotal = $product['priceTotal'];
           array_push($ret['productsnew'],array(
               'allMaterialsWithDetails'=>$allMaterialsWithDetails,
               'productDetails'=>$productDetails,
               'pricePerItem'=>$pricePerItem,
               'priceTotal'=>$priceTotal,
               'quantity'=>$quantity,
               'diameter'=>$product['diameter']
           ));
           $ret['productsTotal']+=$priceTotal;
       }
       foreach($data['productsSimple'] as $prs)
       {
           $prdS = $this->db->where('id',$prs['product_id'])->get('products_simple')->row();
           array_push($ret['productsSimple'],array(
               'productDetails'=>$prdS,
               'quantity'=>$prs['quantity'],
               'pricePerItem'=>$prs['pricePerItem'],
               'priceTotal'=>$prs['priceTotal']
           ));
           $ret['productsTotal']+=$prs['priceTotal'];
       }

       $shipping = $this->db->where('title <>','') -> get('shipping')->result();
       $ret['shipping']=array('allShipping'=>$shipping,'selectedShipping'=>$data['shipping']);

       $payment = $this->db->where('title <>','') -> get('payment')->result();
       $ret['payment']=array('allPayment'=>$payment,'selectedPayment'=>$data['payment']);

       return $ret;
   }

    public function getShippingInformation($id){
        return $this->db->where('id',$id)->get('shipping')->row();
    }
    public function getPaymentInformation($id){
        return $this->db->where('id',$id)->get('payment')->row();
    }
    public function sendOrder($name,$address1,$city,$tel,$email,$note,$data,$options){
        $data = json_decode($data,true);

        /*echo "<pre>";
        print_r($data); die;*/
      $HTML='<div style="width:500px;">';
      $HTML.='<h2>Данные о покупателе</h2>';
      $HTML.='<table  cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 0;border-collapse: collapse; ">';
      $HTML.='<tr>';
      $HTML.='<td style="border: 1px solid black;padding: 5px; background:Yellow">';
      $HTML.='Имя';
      $HTML.='</td>';
      $HTML.='<td style="border: 1px solid black;padding: 5px;">';
      $HTML.=$name;
      $HTML.='</td>';
      $HTML.='</tr>';
      $HTML.='<tr>';
      $HTML.='<td style="border: 1px solid black;padding: 5px; background:Yellow">';
      $HTML.='Адрес';
      $HTML.='</td>';
      $HTML.='<td style="border: 1px solid black;padding: 5px;">';
      $HTML.=$address1;
      $HTML.='</td>';
      $HTML.='</tr>';
      $HTML.='<tr>';
      $HTML.='<td style="border: 1px solid black;padding: 5px; background:Yellow">';
      $HTML.='Город';
      $HTML.='</td>';
      $HTML.='<td style="border: 1px solid black;padding: 5px;">';
      $HTML.=$city;
      $HTML.='</td>';
      $HTML.='</tr>';
      $HTML.='<tr>';
      $HTML.='<td style="border: 1px solid black;padding: 5px; background:Yellow">';
      $HTML.='Телефон';
      $HTML.='</td>';
      $HTML.='<td style="border: 1px solid black;padding: 5px;">';
      $HTML.=$tel;
      $HTML.='</td>';
      $HTML.='</tr>';
      $HTML.='<tr>';
      $HTML.='<td style="border: 1px solid black;padding: 5px; background:Yellow">';
      $HTML.='Адрес e-mail';
      $HTML.='</td>';
      $HTML.='<td style="border: 1px solid black;padding: 5px;">';
      $HTML.=$email;
      $HTML.='</td>';
      $HTML.='</tr>';
      $HTML.='<tr>';
      $HTML.='<td style="border: 1px solid black;padding: 5px; background:Yellow">';
      $HTML.='Комментария';
      $HTML.='</td>';
      $HTML.='<td style="border: 1px solid black;padding: 5px;">';
      $HTML.=$note;
      $HTML.='</td>';
      $HTML.='</tr>';
      $HTML.='</table>';
      $HTML.='<h2>Продукты</h2>';
$total = 0;
        //not simple products table
        $HTML.='<table  cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 0;border-collapse: collapse; ">';
        $HTML.='<thead>';
        $HTML.='<tr>';
        $HTML.='<td style="border: 1px solid black;padding: 5px; background:Yellow">';
        $HTML.='Описание / Параметры';
        $HTML.='</td>';
        $HTML.='<td style="min-width: 100px;border: 1px solid black;padding: 5px;background:Yellow">';
        $HTML.='Цена';
        $HTML.='</td>';
        $HTML.='<td style="border: 1px solid black;padding: 5px;background:Yellow">';
        $HTML.='Количество';
        $HTML.='</td>';
        $HTML.='<td style="width: 15%;border: 1px solid black;padding: 5px;background:Yellow">';
        $HTML.='ПодИтог';
        $HTML.='</td>';
        $HTML.='</tr>';
        $HTML.='</thead>';
        $HTML.='<tbody>';
//        echo "<pre>"; print_r($data['products']); die;
        foreach($data['products'] as $product){
            $prDet = $this->db->where('id',$product['product_id'])->get('products')->row();
            $HTML.='<tr>';
            $HTML.='<td style="border: 1px solid black;padding: 5px;">';
            $HTML.='<span>';
            $HTML.=$prDet->title.' |';
            if($prDet->id!=43 && $prDet->id!=44)//Экран 0,5 мм, Консоль
             $HTML.='Диаметр: '.$product['diameter'].' мм | ';
            $HTML.=$this->db->where('id',$product['material_id'])->get('materials')->row()->long_description;
            $HTML.='</span>';
            $HTML.='</td>';
            $HTML.='<td style="border: 1px solid black;padding: 5px;">';

            $HTML.='<span>';
            $HTML.=$product['pricePerItem'];
            $HTML.='</span>';
            $HTML.='</td>';
            $HTML.='<td style="border: 1px solid black;padding: 5px;">';

            $HTML.='<span>';
            $HTML.=$product['quantity'];
            $HTML.='</span>';
            $HTML.='</td>';
            $HTML.='<td style="border: 1px solid black;padding: 5px;">';
            $HTML.='<span>';
            $total+=$product['pricePerItem']*$product['quantity'];
            $HTML.=$product['pricePerItem']*$product['quantity'];
            $HTML.='</span>';
            $HTML.='</td>';

            $HTML.='</tr>';
        }
//        $HTML.='</tbody>';
//        $HTML.='</table>';

        //end not simple products table
      $HTML.="<h1>&nbsp;</h1>";
        //new products table
//        $HTML.='<table  cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 0;border-collapse: collapse; ">';
//        $HTML.='<thead>';
//        $HTML.='<tr>';
//        $HTML.='<td style="border: 1px solid black;padding: 5px; background:Yellow">';
//        $HTML.='Описание / Параметры';
//        $HTML.='</td>';
//        $HTML.='<td style="min-width: 100px;border: 1px solid black;padding: 5px;background:Yellow">';
//        $HTML.='Цена';
//        $HTML.='</td>';
//        $HTML.='<td style="border: 1px solid black;padding: 5px;background:Yellow">';
//        $HTML.='Количество';
//        $HTML.='</td>';
//        $HTML.='<td style="width: 15%;border: 1px solid black;padding: 5px;background:Yellow">';
//        $HTML.='ПодИтог';
//        $HTML.='</td>';
//        $HTML.='</tr>';
//        $HTML.='</thead>';
//        $HTML.='<tbody>';
//        echo "<pre>"; print_r($data['products']); die;
        foreach($data['productsnew'] as $product){
            $prDet = $this->db->where('id',$product['product_id'])->get('productsnew')->row();
            $HTML.='<tr>';
            $HTML.='<td style="border: 1px solid black;padding: 5px;">';
            $HTML.='<span>';
            if ($prDet->id == 9) {////бак круглый на трубе
                if($product['diameter'] == 1){
                    $HTML.=$prDet->title.' | объем:  45 л.';
                }
                elseif($product['diameter'] == 2){
                    $HTML.=$prDet->title.' | объем:  72 л.';
                }
            }

            elseif ($prDet->id == 10) {//бак квадратный на трубе
                if($product['diameter'] == 1){
                    $HTML.=$prDet->title.' | объем:  55 л.';
                }
                elseif($product['diameter'] == 2){
                    $HTML.=$prDet->title.' | объем:  73 л.';
                }
            }

            elseif ($prDet->id == 11) {//теплообменнык
                if($product['diameter'] == 1){
                    $HTML.=$prDet->title.' | объем:  7 л.';
                }
                elseif($product['diameter'] == 2){
                    $HTML.=$prDet->title.' | объем:  12 л.';
                }
            }

            elseif ($prDet->id == 12) {//теплообменнык
                if($product['diameter'] == 1){
                    $HTML.=$prDet->title.' | объем:  15 л.';
                }
                elseif($product['diameter'] == 2){
                    $HTML.=$prDet->title.' | объем:  20 л.';
                }
            }
            else
            $HTML.=$prDet->title.' | Диаметр:  '.$product['diameter'].' мм ';
            $HTML.='</span>';
            $HTML.='</td>';
            $HTML.='<td style="border: 1px solid black;padding: 5px;">';

            $HTML.='<span>';
            $HTML.=$product['pricePerItem'];
            $HTML.='</span>';
            $HTML.='</td>';
            $HTML.='<td style="border: 1px solid black;padding: 5px;">';

            $HTML.='<span>';
            $HTML.=$product['quantity'];
            $HTML.='</span>';
            $HTML.='</td>';
            $HTML.='<td style="border: 1px solid black;padding: 5px;">';

            $HTML.='<span>';
            $total+=$product['pricePerItem']*$product['quantity'];
            $HTML.=$product['pricePerItem']*$product['quantity'];
            $HTML.='</span>';
            $HTML.='</td>';

            $HTML.='</tr>';
        }
        $HTML.='</tbody>';
        $HTML.='</table>';

        //end new products table



        $HTML.='<h2>Способ Доставки</h2>';
        //start shipping table
        $HTML.='<table  cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 0;border-collapse: collapse; ">';
        $HTML.='<tr>';
        $HTML.='<td style="border: 1px solid black;padding: 5px; background:Yellow">';
        $HTML.='Имя';
        $HTML.='</td>';
        $HTML.='<td style="border: 1px solid black;padding: 5px;">';
        $HTML.=$this->getShippingInformation($data['shipping'])->title;
        $HTML.='</td>';
        $HTML.='</tr>';
        $HTML.='<tr>';
        $HTML.='<td style="border: 1px solid black;padding: 5px; background:Yellow">';
        $HTML.='Стоимость';
        $HTML.='</td>';
        $HTML.='<td style="border: 1px solid black;padding: 5px;">';
        $total+=$this->getShippingInformation($data['shipping'])->price;
        $HTML.=$this->getShippingInformation($data['shipping'])->price;
        $HTML.='</td>';
        $HTML.='</tr>';

        $HTML.='</table>';


        $HTML.='<h2>Способ Оплаты</h2>';
        //start payment table
        $HTML.='<table  cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 0;border-collapse: collapse; ">';
        $HTML.='<tr>';
        $HTML.='<td style="border: 1px solid black;padding: 5px; background:Yellow">';
        $HTML.='Имя';
        $HTML.='</td>';
        $HTML.='<td style="border: 1px solid black;padding: 5px;">';
        $HTML.=$this->getPaymentInformation($data['payment'])->title;
        $HTML.='</td>';
        $HTML.='</tr>';
        $HTML.='<tr>';
        $HTML.='<td style="border: 1px solid black;padding: 5px; background:Yellow">';
        $HTML.='Стоимость';
        $HTML.='</td>';
        $HTML.='<td style="border: 1px solid black;padding: 5px;">';
        $total+=$this->getPaymentInformation($data['payment'])->price;
        $HTML.=$this->getPaymentInformation($data['payment'])->price;
        $HTML.='</td>';
        $HTML.='</tr>';
        $HTML.='</table>';



        $HTML.='<h2>Итого</h2>';
        //start payment table
        $HTML.='<table  cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 0;border-collapse: collapse; ">';
        $HTML.='<tr>';
        $HTML.='<td style="border: 1px solid black;padding: 5px; background:Yellow">';
        $HTML.=$total. ' руб';
        $HTML.='</td>';
        $HTML.='</tr>';
        $HTML.='</table>';

        $HTML.='</div>';
       $config = [
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.yandex.com',
                'smtp_port' => 465,
                'smtp_user' => 'server@stahlbaum.ru',
                'smtp_pass' => 'stahlbaum',
                'mailtype'  => 'html', 
                'charset'   => 'utf-8'
            ];
            $this->load->library('email', $config);
        $this->load->library('email');
        $email_setting  = array('mailtype'=>'html');
        $this->email->initialize($email_setting);
        $this->email->from($this->config->item('server_email'), '');
        $this->email->to($options->sales_email1);
        $this->email->subject('Заказ от посетителя: '.$email);
        $this->email->message($HTML);
        if($this->email->send()){
            //Success email Sent
            //echo $this->email->print_debugger();
        }else{
            //Email Failed To Send
            //echo $this->email->print_debugger();
        }
    }

}