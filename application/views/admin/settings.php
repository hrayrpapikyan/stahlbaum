<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $pageSubTitle;?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $formTitle;?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <?php
                            if (isset($scsMsg)){
                                echo '<p class="alert alert-success">'.$scsMsg.'</p>';
                            }
                            ?>
                            <?php echo form_open('admin/settings', array("role"=>'form'));?>
                            <div class="form-group">
                                <label>Новый логин</label>
                                <?php echo form_input(array("name"=>'username',"class"=>"form-control","value"=>set_value('username')));?>
                                <?php echo form_error('username', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label>Действующий пароль</label>
                                <?php echo form_input(array("name"=>'passwordCurrent',"class"=>"form-control","type"=>"password"));?>
                                <?php echo form_error('passwordCurrent', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label>Новый пароль</label>
                                <?php echo form_input(array("name"=>'password',"class"=>"form-control","type"=>"password"));?>
                                <?php echo form_error('password', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label>Повторить пароль</label>
                                <?php echo form_input(array("name"=>'passwordConfirm',"class"=>"form-control","type"=>"password"));?>
                                <?php echo form_error('passwordConfirm', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <button type="submit" class="btn btn-default">Вход</button>

                            </form>
                        </div>

                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->