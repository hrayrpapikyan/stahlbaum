<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $pageSubTitle;?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $formTitle;?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php echo form_open_multipart('admin/addproductsimple/'.$categoryInfo['id'], array("role"=>'form'));?>

                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>Имя Продукта</label>
                                <?php echo form_input(array("name"=>'product_name',"class"=>"form-control","value"=>set_value('product_name')));?>
                                <?php echo form_error('product_name', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label>Цена Продукта</label>
                                <?php echo form_input(array("name"=>'price',"class"=>"form-control","value"=>set_value('price')));?>
                                <?php echo form_error('price', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>


                            <div class="form-group">
                                <label>Слэг</label>
                                <?php echo form_input(array("name"=>'slugname',"class"=>"form-control","value"=>set_value('slugname')));?>
                                <?php echo form_error('slugname', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>


                            <div class="form-group">
                                <label>Картинка (Ширина и высота должны быть ровны)</label>
                                <?php echo form_input(array("name"=>'userfile',"class"=>"form-control",'type'=>'file'));?>
                                <?php if(isset($userfileerror)) {echo '<p class="help-block alert alert-danger">'.$userfileerror.'</p>';} ?>
                            </div>

                            <div class="form-group">
                                <label>Открывающаяся Картинка (любых размеров)</label>
                                <?php echo form_input(array("name"=>'userfile1',"class"=>"form-control",'type'=>'file'));?>
                                <?php if(isset($userfile1error)) {echo '<p class="help-block alert alert-danger">'.$userfile1error.'</p>';} ?>

                            </div>



                            <button type="submit" class="btn btn-default" name="submit">Добавить</button>

                        </div>
                        </form>

                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->