<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $pageSubTitle;?></h1>
        </div>
        <?php if(isset($error)){
        var_dump($error); die;
    }?>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $formTitle;?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-10">
                            <?php echo form_open('admin/'.$actionName, array("role"=>'form'));?>

                            <div class="form-group">
                                <label>Описание</label>
                                <textarea class="form-control tiny-custom" name="pagecontent" rows="10"><?php echo $pageContent ?></textarea>
                            </div>

                            <button type="submit" class="btn btn-default">Сохранить</button>

                            </form>
                        </div>

                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->