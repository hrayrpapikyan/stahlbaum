<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="<?php echo site_url('admin/options');?>"><i class="fa fa-gear fa-fw"></i>Настройки</a>
            </li>


            <li>
                <a href="<?php echo site_url('admin/cartshipping');?>"><i class="fa fa-home fa-fw"></i>Доставка</a>
            </li>

            <li>
                <a href="<?php echo site_url('admin/cartpayment');?>"><i class="fa fa-money fa-fw"></i>Оплата</a>
            </li>

            <li>
                <a href="<?php echo site_url('admin/slider');?>"><i class="fa fa-picture-o fa-fw"></i>Слайдер</a>
            </li>



            <li>
                <a href="#"><i class="fa fa-file-text fa-fw"></i>Статические страницы<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo site_url('admin/aboutus');?>"><i class="fa fa-info-circle fa-fw"></i> О Нас</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/tocustomers');?>"><i class="fa fa-info-circle fa-fw"></i> Покупателям</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/shipping');?>"><i class="fa fa-info-circle fa-fw"></i> Оплата и Доставка</a>
                    </li>

                </ul>
                <!-- /.nav-second-level -->
            </li>

            <li>
                <a href="<?php echo site_url('admin/producthierarchy');?>"><i class="fa fa-shopping-cart fa-fw"></i>Категории и продукты</a>
            </li>


        </ul>
        <!-- /#side-menu -->
    </div>
    <!-- /.sidebar-collapse -->
</nav>
<!-- /.navbar-static-side -->