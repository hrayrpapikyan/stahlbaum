<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title; ?>
    </title>
    <!-- Core CSS - Include with every page -->
    <link href="<?php echo base_url();?>css/admin/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="<?php echo base_url();?>css/admin/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/admin/plugins/timeline/timeline.css" rel="stylesheet">
    <!-- SB Admin CSS - Include with every page -->
    <link href="<?php echo base_url();?>css/admin/sb-admin.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/admin/admin-custom.css" rel="stylesheet">
    <!-- Core Scripts - Include with every page -->
    <script src="<?php echo base_url();?>js/admin/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url();?>js/admin/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/admin/plugins/metisMenu/jquery.metisMenu.js"></script>
    <!-- Page-Level Plugin Scripts - Dashboard -->
    <!-- SB Admin Scripts - Include with every page -->
    <script src="<?php echo base_url();?>js/admin/sb-admin.js"></script>
    <!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
    <script type="text/javascript" src="<?php echo base_url();?>js/admin/tinymce/tinymce.min.js"></script>
    <script> base_url="<?php echo base_url();?>"</script>

</head>
<body>
<div id="wrapper">