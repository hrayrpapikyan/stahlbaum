<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $pageSubTitle;?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $formTitle;?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php echo form_open_multipart('admin/addcategorynew/'.$superCategoryId, array("role"=>'form'));?>

                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>Имя подкатегории</label>
                                <?php echo form_input(array("name"=>'category_name',"class"=>"form-control","value"=>set_value('category_name')));?>
                                <?php echo form_error('category_name', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>


                            <div class="form-group">
                                <label>Слэг</label>
                                <?php echo form_input(array("name"=>'slug_name',"class"=>"form-control","value"=>set_value('slug_name')));?>
                                <?php echo form_error('slug_name', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label>Диаметры</label>
                                <?php echo form_input(array("name"=>'diameters',"class"=>"form-control","value"=>set_value('diameters')));?>
                                <?php echo form_error('diameters', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label>Картинка (Ширина и высота должны быть ровны)</label>
                                <?php echo form_input(array("name"=>'userfile',"class"=>"form-control",'type'=>'file'));?>
                                <?php if(isset($error)) {echo '<p class="help-block alert alert-danger">'.$error.'</p>';} ?>
                            </div>


                            <button type="submit" class="btn btn-default">Добавить</button>

                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Описание</label>
                                <textarea class="form-control tiny-custom" name="description" rows="10"><?php set_value('description'); ?><?php echo set_value('description'); ?></textarea>
                            </div>
                        </div>
                        </form>

                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->