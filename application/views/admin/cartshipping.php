<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $pageSubTitle;?></h1>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $formTitle;?>
                </div>
                <div class="panel-body">

                        <?php echo form_open_multipart('admin/cartshipping', array("role"=>'form'));?>

                    <?php foreach($shippingData as $item) { ?>
                    <div class="row" style="border-bottom:2px solid Black">
                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>Описание</label>
                                <textarea class="form-control " name="description[]" rows="3"><?php echo $item->description ?></textarea>
                            </div>

                        </div>
                        <div class="col-lg-6" >

                            <div class="form-group">
                                <label>Название</label>
                                <?php echo form_input(array("name"=>'title[]',"class"=>"form-control","value"=>$item->title));?>

                            </div>


                            <div class="form-group">
                                <label>Цена</label>
                                <?php echo form_input(array("name"=>'price[]',"class"=>"form-control","value"=>$item->price));?>

                            </div>




                        </div>
                    </div>



<?php } ?>


                    <div class="row">
                        <button type="submit" class="btn btn-default" name="submit">Добавить</button>
                    </div>
                    <!-- /.row (nested) -->
                        </form>

                        <!-- /.col-lg-6 (nested) -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->