<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Административная система</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Вход в административную систему
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                         <?php echo form_open('admin_login', array("role"=>'form'));?>
                            <div class="form-group">
                                <label>Логин</label>
                                <?php echo form_input(array("name"=>'username',"class"=>"form-control","value"=>set_value('username')));?>
                                <?php echo form_error('username', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label>Пароль</label>
                                <?php echo form_input(array("name"=>'password',"class"=>"form-control","type"=>"password"));?>
                                <?php echo form_error('password', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <button type="submit" class="btn btn-default">Вход</button>

                        </form>
                    </div>

                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->