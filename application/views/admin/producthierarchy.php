<div id="page-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $pageSubTitle;?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $formTitle;?>
                </div>
                <div class="panel-body">
                    <div class="row">

                        <a href="<?php echo site_url('admin/addsupercategory');?>"><i class="fa fa-plus-circle fa-fw"></i>Добавить Категорию</a>                        <!-- /.col-lg-6 (nested) -->

                        <div style="width:100%;height:15px;"></div>


                    </div>
                    <div class="row" style="background:grey">
                        <div class="col-lg-2" style="color:red">
                            Красний - категории
                        </div>
                        <div class="col-lg-3" style="color:blue">
                            Синий - Подкатегории с диаметрамы и материаламы
                        </div>

                        <div class="col-lg-3" style="color:Orange">
                            Оранжевый - Подкатегории с диаметрамы, но без  материалов
                        </div>
                        <div class="col-lg-2" style="color:Green">
                            Зеленый - Продукты
                        </div>


                    </div>
                    <div class="row">
                        <div style="height:15px;">&nbsp;</div>
                    </div>
                    <?php for($i=0;$i<count($hierarchy);$i++) {?>
                    <div class="row supercategory" >
                       <div class="col-lg-4" >
                           <div >
                               <?php echo $hierarchy[$i]['supercategory']->title;?>
                           </div>
                       </div>
                    <div class="col-lg-3">
                        <a href="<?php echo site_url('admin/addcategory/'.$hierarchy[$i]['supercategory']->id);?>"><i class="fa fa-plus fa-fw"></i>Добавить подкатегорию  диаметрамы и материаламы</a>
                     </div>

                        <div class="col-lg-3" >
                            <a href="<?php echo site_url('admin/addcategorynew/'.$hierarchy[$i]['supercategory']->id);?>"><i class="fa fa-plus fa-fw"></i>Добавить подкатегорию с диаметрамы но без материалов</a>
                        </div>
                        <div class="col-lg-1" >
                            <a href="<?php echo site_url('admin/editsupercategory/'.$hierarchy[$i]['supercategory']->id);?>"><i class="fa fa-edit fa-fw"></i></a>
                        </div>
                        <div class="col-lg-1" >
                            <a data-supercategory="<?php echo $hierarchy[$i]['supercategory']->id;?>" class="supercategory-link" href="#">X</a>
                        </div>
                    </div>

                        <!--categories with diameters and materials-->

                        <?php foreach($hierarchy[$i]['categories'] as $catDM){?>
                        <div class="row category" >
                            <div class="col-lg-2" >----</div>
                            <div class="col-lg-6" ><?php echo $catDM->title;?></div>
                            <div class="col-lg-2" >
                                <a href="<?php echo site_url('admin/addproduct/'.$catDM->id);?>"><i class="fa fa-plus fa-fw">Добавить продукт</i></a>
                            </div>

                            <div class="col-lg-1" >
                                <a href="<?php echo site_url('admin/editCategory/'.$catDM->id);?>"><i class="fa fa-edit fa-fw"></i></a>
                            </div>
                            <div class="col-lg-1" >
                                <a class="category-link" href="#" data-category="<?php echo $catDM->id?>">X</a>
                            </div>
                        </div>
                         <?php foreach($hierarchy[$i]['products'][$catDM->id] as $pr){?>
                            <div class="row product" >
                                <div class="col-lg-4" >--------</div>
                            <div class="col-lg-6" ><?php echo $pr->title;?></div>

                            <div class="col-lg-1" >
                                <a href="<?php echo site_url('admin/editproduct/'.$pr->id);?>"><i class="fa fa-edit fa-fw"></i></a>
                            </div>
                            <div class="col-lg-1" >
                                <a data-product="<?php echo $pr->id?>" class="product-link" href="#">X</a>
                            </div>
                            </div>
                         <?php } ?>
                      <?php }?>

                    <!--end categories with diameters and materials-->

                    <!--categories with diameters and without materials-->

                    <?php foreach($hierarchy[$i]['categoriesnew'] as $catDM){?>
                        <div class="row categorynew" >
                            <div class="col-lg-2" >----</div>
                            <div class="col-lg-6" ><?php echo $catDM->title;?></div>
                            <div class="col-lg-2" >
                                <a href="<?php echo site_url('admin/addproductnew/'.$catDM->id);?>"><i class="fa fa-plus fa-fw">Добавить продукт</i></a>
                            </div>

                            <div class="col-lg-1" >
                                <a href="<?php echo site_url('admin/editcategorynew/'.$catDM->id);?>"><i class="fa fa-edit fa-fw"></i></a>
                            </div>
                            <div class="col-lg-1" >
                                <a class="category-link-new" href="#" data-category="<?php echo $catDM->id?>">X</a>
                            </div>
                        </div>
                        <?php foreach($hierarchy[$i]['productsnew'][$catDM->id] as $pr){?>
                            <div class="row productnew" >
                                <div class="col-lg-4" >--------</div>
                                <div class="col-lg-6" ><?php echo $pr->title;?></div>

                                <div class="col-lg-1" >
                                    <a href="<?php echo site_url('admin/editproductnew/'.$pr->id);?>"><i class="fa fa-edit fa-fw"></i></a>
                                </div>
                                <div class="col-lg-1" >
                                    <a data-product="<?php echo $pr->id?>" class="product-linknew" href="#">X</a>
                                </div>
                            </div>
                            <?php } ?>
                        <?php }?>

                    <!--end categories with diameters and without materials-->



                    <?php } ?>

                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->