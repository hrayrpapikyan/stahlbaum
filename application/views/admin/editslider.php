<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $pageSubTitle;?></h1>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $formTitle;?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php echo form_open_multipart('admin/editslider/'.$sliderInfo->id, array("role"=>'form'));?>

                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>Имя Слайда</label>
                                <?php echo form_input(array("name"=>'title',"class"=>"form-control","value"=>$sliderInfo->title));?>
                                <?php echo form_error('title', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>


                            <div class="form-group">
                                <label>Подобзацс</label>
                                <?php echo form_input(array("name"=>'subtitle',"class"=>"form-control","value"=>$sliderInfo->subtitle));?>

                            </div>

                            <div class="form-group">
                                <label>Описание</label>
                                <textarea class="form-control tiny-custom" name="description" rows="5"><?php echo $sliderInfo->description; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Картинка (725x280)</label>
                                <?php echo form_input(array("name"=>'userfile',"class"=>"form-control",'type'=>'file'));?>
                                <?php if(isset($userfileerror)) {echo '<p class="help-block alert alert-danger">'.$userfileerror.'</p>';} ?>
                            </div>

                            <?php if($sliderInfo->thumb!='') {?>
                            <div class="form-group">
                                <img alt="" src="<?php echo base_url()?>uploads/slider/<?php echo $sliderInfo->thumb;?> " >
                            </div>
                            <?php } ?>




                            <button type="submit" class="btn btn-default" name="submit">Сохранить</button>

                        </div>

                        </form>

                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->