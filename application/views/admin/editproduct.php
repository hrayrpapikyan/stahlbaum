<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $pageSubTitle;?></h1>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $formTitle;?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php echo form_open_multipart('admin/editproduct/'.$productInfo['product']->id, array("role"=>'form'));?>

                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>Имя Продукта</label>
                                <?php echo form_input(array("name"=>'product_name',"class"=>"form-control","value"=>$productInfo['product']->title));?>
                                <?php echo form_error('product_name', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>


                            <div class="form-group">
                                <label>Описание</label>
                                <?php echo form_input(array("name"=>'slugname',"class"=>"form-control","value"=>$productInfo['product']->slug));?>
                                <?php echo form_error('slugname', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>


                            <div class="form-group">
                                <label>Картинка (Ширина и высота должны быть ровны)</label>
                                <?php echo form_input(array("name"=>'userfile',"class"=>"form-control",'type'=>'file'));?>
                                <?php if(isset($userfileerror)) {echo '<p class="help-block alert alert-danger">'.$userfileerror.'</p>';} ?>
                            </div>

                            <?php if($productInfo['product']->img_main!='') {?>
                            <div class="form-group">
                                <img alt="" src="<?php echo base_url()?>uploads/products/thumb/<?php echo $productInfo['product']->img_main?> " style="width:204px;height:204px;">
                            </div>
                            <?php } ?>

                            <div class="form-group">
                                <label>Открывающаяся Картинка (любых размеров)</label>
                                <?php echo form_input(array("name"=>'userfile1',"class"=>"form-control",'type'=>'file'));?>
                                <?php if(isset($userfile1error)) {echo '<p class="help-block alert alert-danger">'.$userfile1error.'</p>';} ?>

                            </div>

                            <?php if($productInfo['product']->img_more!='') {?>
                            <div class="form-group">
                                <img alt="" src="<?php echo base_url()?>uploads/products/popup/<?php echo $productInfo['product']->img_more?> " style="max-width:204px;max-height:204px;" >
                            </div>
                            <?php } ?>



                            <button type="submit" class="btn btn-default" name="submit">Добавить</button>

                        </div>
                        <div class="col-lg-6" id="materials_area">




                            <h1 class="keeper" data-product_id="<?php echo $productInfo['product']->id?>">Материалы  <span class="add-material-edit"><i class="fa fa-plus-circle fa-fw" style="color:GREEN"></i></span></h1>

                               <?php foreach($productInfo['materialsForProduct'] as $matr) {?>
                                 <div class="form-group" style="border:1px solid blue;border-radius:10px;padding:5px;">
                                     <input type="hidden" name="mataerial_id_keeper[]" value="<?php echo $matr->id?>" class="form-control material-id-keeper">
                                <label>Длинное название</label>
                                <input type="text" name="mataerial_long[]" value="<?php echo $matr->long_description?>" class="form-control">
                                <label>Короткое название</label>
                                <input type="text" name="material_short[]" value="<?php echo $matr->short_description?>" class="form-control">
                                 <?php

                                     foreach($productInfo['materialsForProductWithDiametersAndPrices'][$matr->id] as $matr1){
                                 ?>
                                <label >Цена</label>
                                <input type="text" name="material_price[]" value="<?php echo $matr1['diam']?>-<?php echo $matr1['price'];?>" class="form-control">
                                     <?php } ?>

                                <label class="remove_material_edit"><i class="fa fa-minus-circle fa-fw" style="color:RED"></i> Удалить</label>
                                 </div>
                              <?php } ?>



                        </div>
                        </form>

                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->