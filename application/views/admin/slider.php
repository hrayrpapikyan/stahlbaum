<div id="page-wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $pageSubTitle;?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $formTitle;?>
                </div>
                <div class="panel-body">
                    <div class="row">

                        <a href="<?php echo site_url('admin/addslider');?>"><i class="fa fa-info fa-fw"></i>Добавить</a>                        <!-- /.col-lg-6 (nested) -->

                        <div style="width:100%;height:15px;"></div>


                    </div>


                       <?php foreach($allSlides as $slide) { ?>
                        <div class="row">
                                    <div class="col-lg-12">

                                   <a data-id="<?php echo $slide->id;?>" class="slides" href="<?php echo site_url('admin/editslider/'.$slide->id);?>"> <img class="img-responsive" src="<?php echo base_url()?>uploads/slider/<?php echo $slide->thumb?> " ></a>
                                        <div style="width:100%;height:15px;"></div>
                        </div></div>
                            <div ></div>

                    <?php } ?>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->