<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $pageSubTitle;?></h1>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $formTitle;?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php echo form_open_multipart('admin/editCategory/'.$categoryInfo['id'], array("role"=>'form'));?>
                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>Имя категории</label>
                                <?php echo form_input(array("name"=>'category_name',"class"=>"form-control","value"=>$categoryInfo['title']));?>
                                <?php echo form_error('category_name', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>


                            <div class="form-group">
                                <label>Слэг</label>
                                <?php echo form_input(array("name"=>'slug_name',"class"=>"form-control","value"=>$categoryInfo['slug']));?>
                                <?php echo form_error('slug_name', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>
                              <div>
                                <h1>Диаметры <span class="add-diameter"><i class="fa fa-plus-circle fa-fw" style="color:GREEN"></i></span></h1>

                            <?php foreach ($categoryInfo['diameters'] as $d) { ?>

                            <div class="form-group input-group">
                                <span data-id="<?php echo $d->id?>" class="input-group-addon delete-relation">X</span>
                                <?php echo form_input(array("name"=>'diameters[]',"disabled"=>true, "class"=>"form-control","value"=>$d->diameter));?>

                            </div>

                            <?php } ?>
                                </div>
                            <div class="form-group">
                                <label>Картинка (Ширина и высота должны быть ровны)</label>
                                <?php echo form_input(array("name"=>'userfile',"class"=>"form-control",'type'=>'file'));?>
                                <?php if(isset($error)) {echo '<p class="help-block alert alert-danger">'.$error.'</p>';} ?>
                            </div>
                            <?php if($categoryInfo['thumb']!='') {?>
                            <div class="form-group">
                                <img alt="" src="<?php echo base_url()?>uploads/categories/<?php echo $categoryInfo['thumb']?> " style="width:267px;height:267px;">
                            </div>
                            <?php } ?>



                            <button type="submit" class="btn btn-default">Сохранить</button>

                        </div>
                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>Описание</label>
                                <textarea class="form-control tiny-custom" name="description" rows="5"><?php echo $categoryInfo['description']; ?></textarea>
                            </div>
                      </div>
                        </form>

                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->