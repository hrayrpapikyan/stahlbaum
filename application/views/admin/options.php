<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $pageSubTitle;?></h1>
        </div>
        <?php if(isset($error)){
        var_dump($error); die;
    }?>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $formTitle;?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <?php echo form_open('admin/options', array("role"=>'form'));?>

                            <div class="form-group">
                                <label>Имя сайта*</label>
                                <?php echo form_input(array("name"=>'title',"class"=>"form-control","value"=>$options->title));?>
                                <?php echo form_error('title', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label>Meta keywords*</label>
                                <textarea class="form-control" name="meta_keyw" rows="3"><?php echo $options->meta_keyw; ?></textarea>
                                <?php echo form_error('meta_keyw', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label>Meta description*</label>
                                <textarea class="form-control" name="meta_desc" rows="3"><?php echo $options->meta_desc; ?></textarea>
                                <?php echo form_error('meta_desc', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label>Первый телефонный номер*</label>
                                <?php echo form_input(array("name"=>'tel1',"class"=>"form-control","value"=>$options->tel1));?>
                                <?php echo form_error('tel1', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>



                            <div class="form-group">
                                <label>Второй телефонный номер</label>
                                <?php echo form_input(array("name"=>'tel2',"class"=>"form-control","value"=>$options->tel2));?>
                                <?php echo form_error('tel2', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label>Первый адрес*</label>
                                <textarea class="form-control tiny-custom" name="address1" rows="3"><?php echo $options->address1; ?></textarea>
                                <?php echo form_error('address1', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label>Второй адрес</label>
                                <textarea class="form-control tiny-custom" name="address2" rows="3"><?php echo $options->address2; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label>Контактный email*</label>
                                <?php echo form_input(array("name"=>'contacts_email',"class"=>"form-control","value"=>$options->contacts_email));?>
                                <?php echo form_error('contacts_email', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label>email для заказов </label>
                                <?php echo form_input(array("name"=>'sales_email1',"class"=>"form-control","value"=>$options->sales_email1));?>
                                <?php echo form_error('sales_email1', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>


                            <div class="form-group">
                                <label>карта*</label>
                                <textarea class="form-control" name="map" rows="6"><?php echo htmlspecialchars($options->map); ?></textarea>
                                <?php echo form_error('map', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>

                            <div class="form-group">
                                <label>Текст Футера*</label>
                                <textarea class="form-control tiny-custom" name="footer_text" rows="3"><?php echo $options->footer_text; ?></textarea>
                                <?php echo form_error('footer_text', '<p class="help-block alert alert-danger">', '</p>'); ?>
                            </div>


                            <button type="submit" class="btn btn-default">Сохранить</button>

                            </form>
                        </div>

                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->