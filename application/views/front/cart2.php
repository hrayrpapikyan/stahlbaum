<div id="content" class="pull-left">

    <div class="page-title">
        Шаг 2 - данные о покупателе
    </div>


    <div class="clear5"></div>


    <!--second step form-->
    <form method="post" id="cart-second-step-form">
        <table class="userForm" style="margin: 0px auto; width:100%;">
            <caption>Адрес по накладной</caption>
            <tbody>
            <tr style="vertical-align: middle;">
                <td style="width: 40%"><label for="Name">Имя* </label></td>
                <td><input name="name" type="Text" id="Name" value="" required="" oninvalid="InvalidMsg(this,'name');" oninput="InvalidMsg(this,'name');"></td>
            </tr>
            <tr style="vertical-align: middle;">
                <td style="width: 40%"><label for="Address1">Адрес </label></td>
                <td><input name="address1" type="Text" id="Address1" value="" class=""></td>
            </tr>
            <tr style="vertical-align: middle;">
                <td style="width: 40%"><label for="City">Город </label></td>
                <td><input name="city" type="Text" id="City" value="" class=""></td>
            </tr>
            <tr style="vertical-align: middle;">
                <td style="width: 40%"><label for="Phone">Телефон* (формат: (495)000000)</label></td>

                <td><input name="tel" type="tel" id="Phone" pattern='[\(]\d{3}[\)]\d{6,7}' required value="" oninvalid="InvalidMsg(this,'tel');" oninput="InvalidMsg(this,'tel');"></td>
            </tr>
            <tr style="vertical-align: middle;">
                <td style="width: 40%"><label for="Email">Адрес e-mail* </label></td>
                <td><input name="email" type="email" id="Email" value="" required oninvalid="InvalidMsg(this,'email');" oninput="InvalidMsg(this,'email');"></td>
            </tr>
            <tr style="vertical-align: middle;">
                <td style="width: 40%"><label for="Note">Комментария </label></td>
                <td><textarea name="note" id="Note" class=""></textarea></td>
            </tr>
            <input type="hidden" id="hdn" value="" name="hdn">
            </tbody>
        </table>

        <!--end second step form-->

        <div class="clear3"></div>

        <div class="buttons-container">
            <input type="button" id="gobck" value="Назад">
            <input name="submit" type="submit" value="Вперед">
        </div>
    </form>
    <div class="clear3"></div>

</div>
<!--#content-->
<div class=clear0></div>

    <script>

        function InvalidMsg(textbox,inputName) {
            if (textbox.value == '') {
                switch(inputName) {
                    case 'name':
                        textbox.setCustomValidity("Поле 'Имя' является обязательным.");
                        break;
                    case 'tel':
                        textbox.setCustomValidity("Поле 'Телефон' является обязательным.");
                        break;
                    case 'email':
                        textbox.setCustomValidity("Поле 'e-mail' является обязательным.");
                        break;

                }

            }
            else if (textbox.validity.typeMismatch){
                switch(inputName) {
                    case 'tel':
                        textbox.setCustomValidity("Поле 'Телефон' должно содержать корректный телефонный номер.");
                        break;
                    case 'email':
                        textbox.setCustomValidity("Поле 'Адрес e-mail' не содержит корректного адреса электронной почты.");
                        break;
                }
            }

            else if (textbox.validity.patternMismatch){
                switch(inputName) {
                    case 'tel':
                        textbox.setCustomValidity("Поле 'Телефон' должно содержать корректный телефонный номер.");
                        break;
                }
            }

            else {
                textbox.setCustomValidity('');
            }
            return true;
        }

        jQuery(document).ready(function() {
            var jjStr=$.cookie('data');
            $('#hdn').val(jjStr);
            $('#gobck').click(function(){
                window.location="<?php echo base_url();?>korzina_1.html";

            })
        })

    </script>
