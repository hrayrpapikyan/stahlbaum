<div id="content" class="pull-left">
    <div class="page-title">
        Контакты
    </div>


    <div class="clear5"></div>
    <div class="left-of-map pull-left">
        <?php echo $options->address1;?>
        <div class="clear5"></div>
        <?php echo $options->address2;?>
    </div>

    <div class="contact-form pull-right">
        <?php echo validation_errors(); ?>
        <?php echo (isset($success))?'<h1 style="color:Green">'.$success.'</h1>':""?>
        <form id="c-form" method="post" action="">
            <input type="text" placeholder="Ваша электронная почта*" name="email" value="<?php echo (isset($success))?"":set_value('email');?>">
            <div class="clear5"></div>
            <textarea placeholder="Написать сообщение*" name="message"><?php echo (isset($success))?"":set_value('message');?></textarea>

            <div class="clear5"></div>
            <button type="submit">Отправить</button>
            <button id="reset-contact">Сброс</button>
        </form>
        <div class="clear3"></div>

    </div>


    <div class=clear0></div>

</div><!--#content-->
<div class=clear0></div>