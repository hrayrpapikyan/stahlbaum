<div id="content" class="pull-left">

    <?php
    foreach($superCategoryInfo as $item) {
        ?>
        <div class="supercategory">
            <h1 class="supercategory_h1"><a href="<?php echo site_url('/supercategory/'.$item->id.'/'.$item->slug.'.html');?>"><?php echo $item->title?></a></h1>

            <a  href="<?php echo site_url('/supercategory/'.$item->id.'/'.$item->slug.'.html');?>">
                <img class="supercategory-thumb" alt="" src="<?php echo base_url();?>uploads/supercategories/<?php echo  $item->thumb;?>" />
            </a>
            <div class="clear5"></div>
            <?php echo $item->description;?>
        </div>
        <div class="clear5"></div>
        <?php } ?>

</div><!--#content-->
<div class=clear0></div>

</div><!--content-left-menu-->
<div class=clear0></div>