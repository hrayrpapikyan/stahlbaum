<div id="content" class="pull-left">
    <div class=clear5></div>
<?php  //print_r($categoryInfo); die;?>
    <div id="product-name-container">
        <?php echo $categoryInfo['title'];?>&nbsp;Диаметр&nbsp;<?php echo str_replace('-','/',$chosenDiam)?>&nbsp;мм
    </div><!--#product-name-container-->

<?php //echo '<pre>';print_r($productsInfo); die; ?>
    <div class=clear5></div>

    <div id="product-diameter-container">
         <?php  foreach ($categoryInfo['diameters'] as $catDiams) { ?>
        <a href="<?php echo site_url('/category/'.$categoryInfo['id'].'/'.str_replace('/','-',$catDiams->diameter).'/'.$categoryInfo['slug'].'.html');?>" class="catalog-item-size <?php if($catDiams->diameter==str_replace('-','/',$chosenDiam)) echo "active-catalog-item-size"?>"><?php echo $catDiams->diameter;?></a>
        <?php } ?>


    </div><!--#product-diameter-container-->
    <div class=clear5></div>
   <?php foreach ($productsInfo as $prInfo) {?>
    <div class="product-item">
        <a class="product-item-popup pull-left" href="<?php echo base_url();?>uploads/products/popup/<?php echo  $prInfo['product']->img_more;?>">
            <img alt="" src="<?php echo base_url();?>uploads/products/thumb/<?php echo  $prInfo['product']->img_main;?>" class="product-item-img"/>
        </a>

        <div class="product-item-content pull-left">
            <p class="product-item-title">
                <?php echo $prInfo['product']->title;?>
                <?php if($prInfo['product']->id!=43 && $prInfo['product']->id!=44) { // Консоль, Экран 0,5 мм?>
                &nbsp;-&nbsp;Диаметр, мм: <?php echo str_replace('-','/',$chosenDiam) ?>
                <?php } ?>
            </p>
            <div class=clear3></div>

            <p style="padding: 0;margin:0;font-size:14px;">
               <i> <?php echo $prInfo['product']->slug;?></i>

            </p>
            <div class=clear3></div>

            <p class="product-item-type">
                <?php if($prInfo['product']->id==43) { //Экран 0,5 мм?>
                Размеры:
               <?php } elseif($prInfo['product']->id==44) {//  Консоль?>
                Тип:
               <?php } else { ?>
                Материалы:
                <?php } ?>
            </p>

            <?php foreach ($prInfo['pricesAndMaterials'] as $pam) { ?>
            <p class="product-item-type">
                <?php echo $pam['mat']->long_description;?>
            </p>


           <?php  } ?>
            <div class=clear3></div>

            <p class="product-item-type">
                <?php if($prInfo['product']->id==43) { //Экран 0,5 мм?>
                Выберите размеры
                <?php } elseif($prInfo['product']->id==44) {//  Консоль?>
                Выберите тип
                <?php } else { ?>
                Выберите материал
                <?php } ?>            </p>

            <p class="product-item-select">
                <select  style="width: 99%; ">
                    <?php foreach ($prInfo['pricesAndMaterials'] as $pam) { ?>
                    <?php //if($pam['prc']->price) {?>
                    <option value="" data-product="<?php echo $prInfo['product']->id ;?>" data-diameter="<?php echo str_replace('-','/',$chosenDiam)?>" data-material="<?php echo $pam['prc']->material_id?>" data-price="<?php echo $pam['prc']->price;?>"><?php echo $pam['mat']->short_description;?></option>
                    <?php //} ?>
                   <?php } ?>

                </select>
            </p>
            <div class="price-quantity-tocart">
                <p class="product-item-price pull-left">
                    <span class="price-itself"><?php echo ($prInfo['pricesAndMaterials'][0]['prc']->price)?$prInfo['pricesAndMaterials'][0]['prc']->price:'Нет в наличии';?></span>
                    <?php if($prInfo['pricesAndMaterials'][0]['prc']->price){ ?>&nbsp;<span class="product-item-currency">руб</span><?php } else { ?><span class="product-item-currency"><small style="color:black">Выберите пожалуйстa другой диаметр, или материал</small></span> <?php } ?>
                </p>
                <div class="quantity-tocart pull-right">
                    <input type="text" class="quantity" value="1" style="visibility:<?php echo ($prInfo['pricesAndMaterials'][0]['prc']->price)?'visible':'hidden'?>" />
                    <button class="to-cart" style="visibility:<?php echo ($prInfo['pricesAndMaterials'][0]['prc']->price)?'visible':'hidden'?>">В корзину</button>
                </div>
                <div class="clear0"></div>
            </div>
        </div><!--.product-item-content-->
        <div class="clear0"></div>
    </div><!--.product-item-->

    <div class="clear5"></div>

        <?php } ?>



</div><!--#content-->
<div class=clear0></div>

</div><!--content-left-menu-->
<div class=clear0></div>