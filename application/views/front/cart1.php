 <div id="content" class="pull-left">


            </div><!--#content-->
            <div class=clear0></div>

                <script>
                   $(document).ready(function(){
                       var jj = $.parseJSON($.cookie('data'));
                       var prodInCards = jj.products;
                       var prodInCardsSimple = jj.productsSimple;
                       var prodInCardsnew = jj.productsnew;
                       if (prodInCards.length==0 && prodInCardsSimple.length==0 && prodInCardsnew.length==0){
                           $('#content').empty().html(
                                   '<div class="page-title" style="text-align: center; margin-top: 20px;">Корзина пуста</div>'+
                                           '<div style="text-align: center; margin-top: 20px;">'+
                                           '<input type="button" value="Продолжить покупки" id="redirectHome">'+
                                           '</div>'
                           );
                       }
                       else{

                           $('#content').html('<img src="<?php echo base_url();?>assets/img/loading.gif" alt="" >');
                         //  alert(13);
                           // alert(JSON.stringify(jj));

                           $.ajax({
                               type: "POST",
                            //   dataType: 'json',
                              // accepts: "application/json; charset=utf-8"
                               url: "<?php echo base_url()?>front/cartinformation",
                               data: {'data':JSON.stringify(jj)}
                           })
                                   .done(function( data ) {
                                       var dataObj=$.parseJSON(data);
                                       console.log(dataObj);

                                       var HTML = '';
                                       HTML +=
             '<div class="page-title">Шаг 1 - выбор метода доставки и оплаты</div>'+
              '<div class="clear5"></div>'  +
               '<div class="page-subtitle"> В таблице перечислены все товары из Корзины и указана суммарная стоимость вашего заказа. </div>'      +
'<table id="cartTable" class="cartTable" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 0;">'+
  '<thead>'    +
  '<tr>'    +
  '<td >Описание / Параметры</td>'    +
  '<td style="min-width: 100px;">Цена</td>'    +
  '<td>Количество</td>'    +
  '<td style="width: 15%;">ПодИтог</td>'    +
  '<td style="width: 6%; text-align: center;" >'    +
  '</td>'    +
  ' </tr>'    +
  ' </thead>'    +
  ' <tbody>';


                                       $.each(dataObj.products, function( index, value ) {

                                           HTML+='<tr class="notSimpleProduct" data-id="'+this.productDetails.id+'" data-diameter="'+this.diameter+'">'+
    '<td class="product-td">'+
    '<span class="product-td-image-name">'+
    '<img src="<?php echo base_url();?>uploads/products/thumb/'+this.productDetails.img_main+'" alt="" class="product-td-image pull-left">'+
    '<span class="product-td-name pull-left">'+
    this.productDetails.title +' Диаметр, мм: '+this.diameter +
    ' </span>'+
    '<span class="clear0" style="display:block"></span>'+
    '</span><!--.product-td-image-name-->'+
    '<span class="clear3" style="display:block"></span>'+
    '<span class="product-td-select-container">'+
    '<select>';
                                           var allM=this.allMaterialsWithDetails;
                                           $.each(allM, function( index, value ) {
                                               HTML+='<option value="" data-price="'+this.price+'" '+' data-material= "'+this.id+'"';
                                               if(this.isChosen){
                                                   HTML+=' selected '
                                               }
                                               HTML+='>'+
                                               this.short_description+
                                               '</option>'

                                           });
                                                   HTML+='</select>'+
                                                           '</span>'+
                                                           '<span class="clear3" style="display:block"></span>'+
                                                           ' <span class="product-description-td-cont">';
//                                                           '<span class="prod-desc-item">Материалы: </span>';
//
//
//                                           $.each(allM, function( index, value ) {
//                                               HTML+='<span class="prod-desc-item">';
//                                              HTML+=this.long_description;
//                                               HTML+='</span>';

//                                           });
//
//                                        HTML +='<span class="clear3" style="display:block"></span>' ;
//                                        HTML +='<span class="prod-desc-item">Выберите материал</span>' ;
                                        HTML +='<span class="clear3" style="display:block"></span>' ;
                                        HTML +=' </span>' ;
                                        HTML +='</td><!--.product-td-->' ;
                                        HTML +='<td class="price-item-td">' ;
                                        HTML +='<span class="price-itself-td">' ;
                                        HTML +=this.pricePerItem ;
                                        HTML +='</span>&nbsp;<span class="currency-td">руб</span></td>' ;
                                        HTML +='<td class="quantity-td">' ;
                                        HTML +='<input type="text" value="'+this.quantity+'" class="quantity-td-input">' ;
                                        HTML +='</td>' ;
                                        HTML +='<td class="sub-price-td">' ;
                                        HTML +='<span class="sub-price-itself-td">' ;
                                        HTML +=this.priceTotal ;
                                        HTML +=' </span>&nbsp;<span class="currency-td">руб</span></td>' ;
                                        HTML +='<td class="delete-td">' ;
                                        HTML +='<img src="<?php echo base_url();?>assets/img/cart-remove.gif" alt="" >' ;
                                        HTML +='</td>' ;
                                        HTML +='</tr><!--tr-->' ;

                                       });

                                       /////////////////new products///////

                                       $.each(dataObj.productsnew, function( index, value ) {
                                                   var productDetailsNewid = this.productDetails.id;
                                           HTML+='<tr class="notSimpleProductNew" data-id="'+this.productDetails.id+'">'+
                                                   '<td class="product-td">'+
                                                   '<span class="product-td-image-name">'+
                                                   '<img src="<?php echo base_url();?>uploads/products/thumb/'+this.productDetails.img_main+'" alt="" class="product-td-image pull-left">'+
                                                   '<span class="product-td-name pull-left">'+
                                                   this.productDetails.title +
                                                   ' </span>'+
                                                   '<span class="clear0" style="display:block"></span>'+
                                                   '</span><!--.product-td-image-name-->'+
                                                   '<span class="clear3" style="display:block"></span>'+
                                                   '<span class="product-td-select-container">'+
                                                   '<select>';
                                           var allM=this.allMaterialsWithDetails;
                                           $.each(allM, function( index, value ) {
                                               HTML+='<option value="" data-price="'+this.price+'" '+' data-diameter= "'+this.diameter+'"';
                                               if(this.isChosen){
                                                   HTML+=' selected ';
                                               }
                                               var thisdiameter=this.diameter;
                                               //alert(productDetailsNewid);
                                               if(productDetailsNewid == 9){//бак круглый на трубе
                                                   if(this.diameter==1){
                                                       thisdiameter=45;
                                                   }
                                                   else if(this.diameter==2){
                                                       thisdiameter=72;
                                                   }
                                               }

                                               if(productDetailsNewid == 10){//бак квадратный на трубе
                                                   if(this.diameter==1){
                                                       thisdiameter=55;
                                                   }
                                                   else if(this.diameter==2){
                                                       thisdiameter=73;
                                                   }
                                               }

                                               if(productDetailsNewid == 11){//теплообменнык
                                                   if(this.diameter==1){
                                                       thisdiameter=7;
                                                   }
                                                   else if(this.diameter==2){
                                                       thisdiameter=12;
                                                   }
                                               }

                                               if(productDetailsNewid == 12){//наливной водонагреватель
                                                   if(this.diameter==1){
                                                       thisdiameter=15;
                                                   }
                                                   else if(this.diameter==2){
                                                       thisdiameter=20;
                                                   }
                                               }
                                               HTML+='>'+
                                                       thisdiameter+
                                                       '</option>'

                                           });
                                           HTML+='</select>'+
                                                   '</span>'+
                                                   '<span class="clear3" style="display:block"></span>'+
                                                   ' <span class="product-description-td-cont">';



                                           HTML +='<span class="clear3" style="display:block"></span>' ;
                                           HTML +=' </span>' ;
                                           HTML +='</td><!--.product-td-->' ;
                                           HTML +='<td class="price-item-td">' ;
                                           HTML +='<span class="price-itself-td">' ;
                                           HTML +=this.pricePerItem ;
                                           HTML +='</span>&nbsp;<span class="currency-td">руб</span></td>' ;
                                           HTML +='<td class="quantity-td">' ;
                                           HTML +='<input type="text" value="'+this.quantity+'" class="quantity-td-input">' ;
                                           HTML +='</td>' ;
                                           HTML +='<td class="sub-price-td">' ;
                                           HTML +='<span class="sub-price-itself-td">' ;
                                           HTML +=this.priceTotal ;
                                           HTML +=' </span>&nbsp;<span class="currency-td">руб</span></td>' ;
                                           HTML +='<td class="delete-td">' ;
                                           HTML +='<img src="<?php echo base_url();?>assets/img/cart-remove.gif" alt="" >' ;
                                           HTML +='</td>' ;
                                           HTML +='</tr><!--tr-->' ;

                                       });
                                       /////end new products//////////






                        //simple products///
                                       $.each(dataObj.productsSimple, function( index, value ) {
                                          // alert(123);
                                           HTML+='<tr class="simpleProducttr" data-id="'+this.productDetails.id+'">'+
                                                   '<td class="product-td">'+
                                                   '<span class="product-td-image-name">'+
                                                   '<img src="<?php echo base_url();?>uploads/products/thumb/'+this.productDetails.img_main+'" alt="" class="product-td-image pull-left">'+
                                                   '<span class="product-td-name pull-left">'+
                                                   this.productDetails.title +
                                                   ' </span>'+
                                                   '<span class="clear0" style="display:block"></span>'+
                                                   '</span><!--.product-td-image-name-->'+
                                                   '<span class="clear3" style="display:block"></span>';
                                                   HTML +='<td class="price-item-td">' ;
                                           HTML +='<span class="price-itself-td">' ;
                                           HTML +=this.pricePerItem ;
                                           HTML +='</span>&nbsp;<span class="currency-td">руб</span></td>' ;
                                           HTML +='<td class="quantity-td">' ;
                                           HTML +='<input type="text" value="'+this.quantity+'" class="quantity-td-input">' ;
                                           HTML +='</td>' ;
                                           HTML +='<td class="sub-price-td">' ;
                                           HTML +='<span class="sub-price-itself-td">' ;
                                           HTML +=this.priceTotal ;
                                           HTML +=' </span>&nbsp;<span class="currency-td">руб</span></td>' ;
                                           HTML +='<td class="delete-td">' ;
                                           HTML +='<img src="<?php echo base_url();?>assets/img/cart-remove.gif" alt="" >' ;
                                           HTML +='</td>' ;
                                           HTML +='</tr><!--tr-->' ;

                                       });
                        //end simple products//

                                       HTML+=' <tr class="products-total-tr">';
                                       HTML+='<td class="invisible"></td>';
                                       HTML+='<td class="dark-green" style="width: 15%;   border-top-color: transparent;">Итого</td>';
                                       HTML+='<td  colspan="2" style="width: 16%;  border-top-color: transparent;">';
                                       HTML+='<span class="products-total-itselt">'+dataObj.productsTotal+'</span>&nbsp;<span>руб</span>';
                                       HTML+='</td>';
                                       HTML+='</tr>';




                                       HTML+='</tbody>';
                                       HTML+='</table>';
                                       ///end products table3

                                       HTML+='<div class="clear3"></div>';
                                       HTML+='<div class="page-subtitle">';
                                       HTML+='Выберите желаемый метод доставки и оплаты, затем нажмите на кнопку "Вперед".';
                                       HTML+='</div>';
                                       HTML+='<div class="clear3"></div>';


                                       //shipping table start
                                       HTML+='<table id="shippingTable" class="cartTable" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 0;">';
                                       HTML+='<thead>';

                                       HTML+='<tr>';
                                       HTML+='<td style="width: 20px" ></td>';
                                       HTML+='<td>Доставка</td>';
                                       HTML+='<td style="width: 22%;" >Цена</td>';
                                       HTML+='</tr>';
                                       HTML+=' <tbody>';
                                       $.each(dataObj.shipping.allShipping, function( index, value ) {
                                           HTML += '<tr>';
                                           HTML += '<td class="shipping-radio-td">';
                                           HTML += ' <input type="radio" name="shipping_method" ';
                                           if(this.id == dataObj.shipping.selectedShipping){
                                               HTML += ' checked ';
                                           }

                                           HTML += 'value="'+this.id+'" ';
                                           HTML += 'data-price="'+this.price+'"';
                                           HTML += '>';
                                           HTML += '</td>';
                                           HTML += '<td class="shipping-text-td">';
                                           HTML += '<span class="shipping-td-title">';
                                           HTML += this.title;
                                           HTML += '</span>';
                                           HTML += '<span class="shipping-td-description">';
                                           HTML += this.description;
                                           HTML += '</span>';
                                           HTML += '</td>';
                                           HTML += '<td class="shipping-price-td">';
                                           HTML += '<span class="shipping-price-itself">';
                                           HTML += this.price;
                                           HTML += '</span>&nbsp;<span>руб</span>';
                                           HTML += '</td>';
                                           HTML += '</tr>';



                                       })
                                       HTML+=' </tbody>';
                                       HTML+=' </table>';
                                      //end shipping table


                                       //payment table start
                                       HTML+=' <div class="clear3"></div>';
                                       HTML+='<table id="paymentTable" class="cartTable" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 0;">';
                                       HTML+='<thead>';
                                       HTML+='<tr>';
                                       HTML+='<td style="width: 20px" ></td>';
                                       HTML+=' <td>Оплата</td>';
                                       HTML+='<td style="width: 22%;" >Цена</td>';
                                       HTML+=' </thead>';
                                       HTML+='<tbody>';
                                       $.each(dataObj.payment.allPayment, function( index, value ) {
                                           HTML += '<tr>';
                                           HTML += '<td class="shipping-payment-td">';
                                           HTML += ' <input type="radio" name="payment_method" ';
                                           if(this.id == dataObj.payment.selectedPayment){
                                               HTML += ' checked ';
                                           }

                                           HTML += 'value="'+this.id+'" ';
                                           HTML += 'data-price="'+this.price+'"';
                                           HTML += '>';
                                           HTML += '</td>';
                                           HTML += '<td class="payment-text-td">';
                                           HTML += '<span class="payment-td-title">';
                                           HTML += this.title;
                                           HTML += '</span>';
                                           HTML += '<span class="payment-td-description">';
                                           HTML += this.description;
                                           HTML += '</span>';
                                           HTML += '</td>';
                                           HTML += '<td class="payment-price-td">';
                                           HTML += '<span class="payment-price-itself">';
                                           HTML += this.price;
                                           HTML += '</span>&nbsp;<span>руб</span>';
                                           HTML += '</td>';
                                           HTML += '</tr>';



                                       })
                                       HTML+='</tbody>';
                                       HTML+='</table>';

                                       //end paymenyt table

                                       HTML+=' <div class="clear3"></div>';


                                       //start total table
                                       HTML+='<table cellpadding="0" cellspacing="0" border="0" class="cartTable">';
                                       HTML+='<tbody>';
                                       HTML+='<tr class="vertical-middle">';
                                       HTML+='<td class="invisible"></td>';
                                       HTML+='<td class="dark-green" style="width:15%; ">Итого</td>';
                                       HTML+='<td  colspan="2" style="width:16%"><span class="extra-total">6160</span>&nbsp;<span>руб</span></td>';
                                       HTML+='</tr>';
                                       HTML+='</tbody>';
                                       HTML+='</table>';
                                       //end total table

                                       HTML+='<div class="clear3"></div>';
                                       HTML+='<div class="buttons-container">';
                                       HTML+='<input type="button" value="Продолжить покупки" id="redirectHome">';
                                       HTML+=' <input type="button" id="btnempty" value="Пустая Корзина">';
                                       HTML+='<input type="button" id="btngonext" value="Вперед">';
                                       HTML+='</div>';
                                       HTML+='<div class="clear3"></div>';




                                       $('#content').html(HTML);

                                       $('.extra-total').text(countCartTotal());


                                   });
                       }

                   })




                   function countCartProductsTotal(){
                       var tp = 0;
                       $( "#cartTable tbody tr" ).not('.products-total-tr').each(function( index ) {
                           tp+=parseInt($(this).find('.sub-price-itself-td').text());
                       });
                       return tp;
                   }

                   function countCartTotal(){
                       var shippingPrice=0;
                       var paymentPrice=0;
                       if($('input[name="shipping_method"]:checked').length>0){
                           shippingPrice=parseInt($('input[name="shipping_method"]:checked').data('price'));
                       }

                       if($('input[name="payment_method"]:checked').length>0){
                           paymentPrice=parseInt($('input[name="payment_method"]:checked').data('price'));
                       }

                       return countCartProductsTotal()+shippingPrice+paymentPrice;
                   }


                   //when quantity of product is changed in the cart
                   $(document).on('blur',".quantity-td-input",function(){
                       if($(this).val()=='' || $(this).val()==0){
                           $(this).val(1);
                       }
                       var _val=$(this).val();
                       var itemPrice=parseInt($(this).parent().parent().find('.price-itself-td').text());
                       $(this).parent().parent().find('.sub-price-itself-td').text(itemPrice*_val);
                       $('.products-total-itselt').text(countCartProductsTotal());
                       $('.extra-total').text(countCartTotal());
                       refreshCookie();
                       var headerCartContent  = countCartAndQuantity();
                       $('#hearder-cart-content-price').text(headerCartContent.cartTotal);
                       $('#hearder-cart-content-quantity').text(headerCartContent.cartQuantity);
                   })

                   //when product material is being changed
                   $(document).on('change',".product-td-select-container select",function(){
                 //  $('.product-td-select-container select').change(function(){
                       var selectedPrice=parseInt($(this).find('option:selected').data('price'));
                       var quantity = parseInt($(this).parent().parent().parent().find('.quantity-td-input').val());
                       $(this).parent().parent().parent().find('.sub-price-itself-td').text(quantity*selectedPrice);
                       $(this).parent().parent().parent().find('.price-itself-td').text(selectedPrice);
                       $('.products-total-itselt').text(countCartProductsTotal());
                       $('.extra-total').text(countCartTotal());
                       refreshCookie();
                       var headerCartContent  = countCartAndQuantity();
                       $('#hearder-cart-content-price').text(headerCartContent.cartTotal);
                       $('#hearder-cart-content-quantity').text(headerCartContent.cartQuantity);

                   })

                   //delete product from cart
                   $(document).on('click',".delete-td img",function(){
                       $(this).parent().parent().remove();

                       $('.products-total-itselt').text(countCartProductsTotal());
                       $('.extra-total').text(countCartTotal());
                       refreshCookie();

                       if($('#cartTable tbody tr').length<=1){
                           $('#content').empty().html(
                                   '<div class="page-title" style="text-align: center; margin-top: 20px;">Корзина пуста</div>'+
                                           '<div style="text-align: center; margin-top: 20px;">'+
                                           '<input type="button" value="Продолжить покупки" id="redirectHome">'+
                                           '</div>'
                           );

                           emptyCookie();

                       }

                       var headerCartContent  = countCartAndQuantity();
                       $('#hearder-cart-content-price').text(headerCartContent.cartTotal);
                       $('#hearder-cart-content-quantity').text(headerCartContent.cartQuantity);
                   })

                   //when shipping method is changed
                   $(document).on('change','input[name="shipping_method"]',function(){
                       $('.extra-total').text(countCartTotal());
                       refreshCookie();
                       var headerCartContent  = countCartAndQuantity();
                       $('#hearder-cart-content-price').text(headerCartContent.cartTotal);
                       $('#hearder-cart-content-quantity').text(headerCartContent.cartQuantity);
                   })

                   //when payment method is changed
                   $(document).on('change','input[name="payment_method"]',function(){
                       $('.extra-total').text(countCartTotal());
                       refreshCookie();
                       var headerCartContent  = countCartAndQuantity();
                       $('#hearder-cart-content-price').text(headerCartContent.cartTotal);
                       $('#hearder-cart-content-quantity').text(headerCartContent.cartQuantity);

                   })

                function refreshCookie(){

                    var data ={};
                    data.products=[];
                    data.productsSimple=[];
                    data.productsnew=[];
                    data.shipping=0;
                    data.payment=0;
                    data.paymentPrice = 0;
                    data.shippingPrice = 0;





                    $( "#cartTable tbody tr.notSimpleProduct" ).each(function( index ) {
                      //  alert($(this).find('.product-td-select-container select option:selected').data('material'));
                        data.products.push({'product_id':$(this).data('id'),'material_id':$(this).find('.product-td-select-container select option:selected').data('material'),'diameter':$(this).data('diameter'),'quantity':$(this).find('.quantity-td-input').val(),'priceTotal':$(this).find('.sub-price-itself-td').text(),'pricePerItem':$(this).find('.price-itself-td').text()});
                    });

                    $( "#cartTable tbody tr.notSimpleProductNew" ).each(function( index ) {
                        //  alert($(this).find('.product-td-select-container select option:selected').data('material'));
                        data.productsnew.push({'product_id':$(this).data('id'),'diameter':$(this).find('.product-td-select-container select option:selected').data('diameter'),'quantity':$(this).find('.quantity-td-input').val(),'priceTotal':$(this).find('.sub-price-itself-td').text(),'pricePerItem':$(this).find('.price-itself-td').text()});
                    });

                    $( "#cartTable tbody tr.simpleProduct" ).each(function( index ) {
                     //   data.products.push({'product_id':$(this).data('id'),'material_id':$(this).find('product-td-select-container select option:selected').data('material'),'diameter':$(this).data('diameter'),'quantity':$(this).find('.quantity-td-input').val(),'priceTotal':$(this).find('.sub-price-itself-td').text(),'pricePerItem':$(this).find('.price-itself-td').text()});
                        data.productsSimple.push({'product_id':$(this).data('id'),'quantity':$(this).find('.quantity-td-input').val(),'priceTotal':$(this).find('.sub-price-itself-td').text(),'pricePerItem':$(this).find('.price-itself-td').text()});
                    });

                    if($('input[name="shipping_method"]:checked').length>0){
                        data.shipping = parseInt($('input[name="shipping_method"]:checked').val());
                        data.shippingPrice=parseInt($('input[name="shipping_method"]:checked').parent().parent().find('.shipping-price-itself').text());

                    }


                    if($('input[name="payment_method"]:checked').length>0){
                        data.payment = parseInt($('input[name="payment_method"]:checked').val());
                        data.paymentPrice=parseInt($('input[name="payment_method"]:checked').parent().parent().find('.payment-price-itself').text());
                    }

                    $.cookie('data',JSON.stringify(data),{ expires: 100, path:'/' });
                    consolelogcookie();
                }

                   //when empty cart is clicked
                   $(document).on('click','#btnempty',function(){
                       if(confirm('Хотите очистить корзину?')){
                           $('#content').empty().html(
                                   '<div class="page-title" style="text-align: center; margin-top: 20px;">Корзина пуста</div>'+
                                           '<div style="text-align: center; margin-top: 20px;">'+
                                           '<input type="button" value="Продолжить покупки" id="redirectHome">'+
                                           '</div>'
                           );

                           emptyCookie();
                           refreshCookie();
                           var headerCartContent  = countCartAndQuantity();
                           $('#hearder-cart-content-price').text(headerCartContent.cartTotal);
                           $('#hearder-cart-content-quantity').text(headerCartContent.cartQuantity);
                       }
                   })
                function emptyCookie(){
                    var data ={};
                    data.products=[];
                    data.productsSimple=[];
                    data.productsnew=[];
                    data.shipping=0;
                    data.payment=0;
                    data.paymentPrice = 0;
                    data.shippingPrice = 0;
                    $.cookie('data',JSON.stringify(data),{ expires: 100, path:'/' });
                    consolelogcookie();
                }


                $(document).on('click','#redirectHome',function(){
                    window.location="<?php echo base_url();?>";

                })

                $(document).on('click','#btngonext',function(){
                    if($('input[name="shipping_method"]:checked').length == 0){
                        alert('Пожалуйста выберите способ доставки');
                    }
                    else{
                        if($('input[name="payment_method"]:checked').length == 0){
                            alert('Пожалуйста выберите способ оплаты');
                        }
                        else
                        window.location="<?php echo base_url();?>korzina_2.html";
                    }

                })
                </script>