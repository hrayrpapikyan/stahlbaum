<div id="header-logo-slider-menu">
    <div id="header-logo-container" class="pull-left">
        <a href="<?php echo site_url('/');?>" id="header-logo">
            <img src="<?php echo base_url();?>assets/img/logo.png" alt="" title="" style="width: 275px;height: 339px;"/>
        </a>
    </div>
    <div id="header-slider-container" class="pull-left">
        <!-- Start WOWSlider.com BODY section -->
        <div id="wowslider-container1">
            <div class="ws_images"><ul>
                <?php foreach ($slider as $sli) { ?>
                <li><img src="<?php echo base_url();?>uploads/slider/<?php echo $sli->thumb;?>" alt="" title="" />
                    <h1 class="slider-h1">
                        <?php echo $sli->title;?>
                    </h1>
                    <h4 class="slider-h4">
                        <?php echo $sli->subtitle;?>
                    </h4>
                    <div class="slider-p">
                        <?php echo $sli->description;?>
                    </div>

                </li>
               <?php } ?>
            </ul></div>
            <div class="ws_bullets"><div>
                                 <?php $i=1;?>   
				                <?php foreach ($slider as $sli) { ?>
								<a href="#" title="<?php echo $i; ?>"><?php echo $i++; ?></a>
                        <?php } ?>
            </div></div>

            <div class="ws_shadow"></div>
        </div>
        <script type="text/javascript" src="<?php echo base_url();?>js/front/slider/wowslider.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>js/front/slider/script.js"></script>
        <!-- End WOWSlider.com BODY section -->
        <div id="main-menu-container" class="pull-left">
            <ul id="main-menu">


                <li><a href="<?php echo site_url('/');?>" class="<?php if($this->router->fetch_method()=='index') {echo 'active-item';} ?>">Каталог</a></li>
                <li><a href="<?php echo site_url('/o_nas.html');?>" class="<?php if($this->router->fetch_method()=='o_nas') {echo 'active-item';} ?>">О нас</a></li>
                <li><a href="<?php echo site_url('/pokupatelyam.html');?>" class="<?php if($this->router->fetch_method()=='pokupatelyam') {echo 'active-item';} ?>">Покупателям</a></li>
                <li><a href="<?php echo site_url('/dostavka_i_oplata.html');?>" class="<?php if($this->router->fetch_method()=='dostavka_i_oplata') {echo 'active-item';} ?>">Доставка и оплата</a></li>
                <li><a href="<?php echo site_url('/korzina_1.html');?>" class="<?php if($this->router->fetch_method()=='korzina_1') {echo 'active-item';} ?>">Ваша карзина</a></li>
                <li><a href="<?php echo site_url('/kontakti.html');?>" class="<?php if($this->router->fetch_method()=='kontakti') {echo 'last-menu-item active-item';} else {echo 'last-menu-item';} ?>" >Контакты</a></li>


            </ul>
        </div><!--#main-menu-container-->
    </div><!--#header-slider-container-->
    <img src="<?php echo base_url();?>assets/img/header-slider-shadow.png" alt="" id="header-slider-shadow"/>
    <img src="<?php echo base_url();?>assets/img/header-left-shadow.png" alt="" id="header-left-shadow"/>
    <img src="<?php echo base_url();?>assets/img/header-right-shadow.png" alt="" id="header-right-shadow"/>
    <div class=clear0></div>

</div><!--#header-logo-slider-menu-->
</div><!--#header-->