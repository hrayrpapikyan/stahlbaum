<div id="to-cart-success" style="display:none">Товар добавлен в корзину</div>

<div id="header">
    <div id="header-top">
        <div class=clear10></div>
        <div id="header-phone" class="pull-left">
            <div id="header-phone-icon" class="pull-left"></div><!--#header-phone-icon-->
            <div id="header-phone-content" class="pull-left">
                <div class="header-phone-item"><?php echo $options->tel1?></div>
                <div class="header-phone-item"><?php echo $options->tel1?></div>
            </div> <!--#header-phone-content-->
        </div><!--#header-phone-->

        <div id="header-cart-container" class="pull-left">
            <div class=clear5></div>

            <a href="<?php echo site_url('/korzina_1.html');?>" id="header-cart-icon" class="pull-left"> </a>
            <div id="header-cart-content" class="pull-left">
                <div id="hearder-cart-content-big">
                    Общая сумма: <span id="hearder-cart-content-price"></span>&nbsp;<span>руб</span>
                </div>
                <div id="hearder-cart-content-small">
                    Всего наименований: <span id="hearder-cart-content-quantity">0</span>
                </div>
            </div> <!--#header-cart-content-->
        </div><!--#header-cart-container-->

        <div id="header-search-and-share" class="pull-right">
            <div id="header-share">
                <div class=clear5></div>
                <div class=clear5></div>
                <div class=clear5></div>
                <div class=clear5></div>
                <div class=clear5></div>
                <div class=clear5></div>
                <div class=clear5></div>
            </div><!--#header-share-->
            <div class=clear5></div>
            <div id="header-search">
                <form method="post" action="<?php echo site_url('front/poisk')?>">
                <button id="header-search-button" class="pull-right" type="submit">
                    <img src="<?php echo base_url();?>assets/img/search-icon.png" class="search-icon" alt=""/>
                </button>
                <input type="text" id="header-search-input" class="pull-right" placeholder="Поиск" name="searchwords">
                </form>
            </div><!--#header-search-->
        </div><!--#header-search-and-share-->
        <div class=clear5></div>
    </div><!--header-top-->