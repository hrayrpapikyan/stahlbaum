<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="description" content="<?php echo $options->meta_desc ;?>" />
    <meta name="keywords" content="<?php echo $options->meta_keyw ;?>" />
    <title> <?php echo $options->title ;?></title>
    <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/img/favicon.png" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/front/slider/style.css" />
    <script type="text/javascript" src="<?php echo base_url();?>js/front/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/front/jquery-cookie.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/front/front-custom.css">
    <!--fancybox-->
    <script type="text/javascript" src="<?php echo base_url();?>js/front/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/front/fancybox/jquery.fancybox-1.3.4.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/front/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    <!--end fancybox-->
    <script type="text/javascript" src="<?php echo base_url();?>js/front/custom-front.js"></script>
</head>
<body>
<div id="container">
    <div id="wrapper">