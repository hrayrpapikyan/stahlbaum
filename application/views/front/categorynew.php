<div id="content" class="pull-left">
    <div class=clear5></div>
<?php  //print_r($categoryInfo); die;?>
    <div id="product-name-container">
        <?php echo $categoryInfo['title']?>
    </div><!--#product-name-container-->

    <div class=clear5></div>

   <?php foreach ($productsInfo as $prInfo) {?>
    <div class="product-item">
        <a class="product-item-popup pull-left" href="<?php echo base_url();?>uploads/products/popup/<?php echo  $prInfo['product']->img_more;?>">
            <img alt="" src="<?php echo base_url();?>uploads/products/thumb/<?php echo  $prInfo['product']->img_main;?>" class="product-item-img"/>
        </a>

        <div class="product-item-content pull-left">
            <p class="product-item-title">
                <?php echo $prInfo['product']->title;?>

            </p>
            <div class=clear3></div>

            <p style="padding: 0;margin:0;font-size:14px;">
                <i> <?php echo $prInfo['product']->slug;?></i>

            </p>
            <div class=clear3></div>

            <p class="product-item-type">
                Выберите объем, литр
            </p>

            <p class="product-item-select">
                <select  style="width: 99%; ">
                    <?php foreach ($prInfo['diametersAndMaterials'] as $pam) { ?>
                    <?php
                    $ddiam = str_replace('-','/',$pam['prc']->diameter);
                    if($prInfo['product']->id==9){//бак круглый на трубе
                        if($ddiam==1){
                            $ddiam=45;
                         }
                        elseif($ddiam==2){
                            $ddiam=72;
                        }
                    }
                    elseif($prInfo['product']->id==10){//бак квадратный на трубе
                        if($ddiam==1){
                            $ddiam=55;
                        }
                        elseif($ddiam==2){
                            $ddiam=73;
                        }
                    }
                    elseif($prInfo['product']->id==11){//теплообменнык
                        if($ddiam==1){
                            $ddiam=7;
                        }
                        elseif($ddiam==2){
                            $ddiam=12;
                        }
                    }
                    elseif($prInfo['product']->id==12){//бак квадратный на трубе
                        if($ddiam==1){
                            $ddiam=15;
                        }
                        elseif($ddiam==2){
                            $ddiam=20;
                        }
                    }
                     ?>
                    <option value="" data-product="<?php echo $prInfo['product']->id ;?>" data-diameter="<?php echo str_replace('-','/',$pam['prc']->diameter)?>"  data-price="<?php echo $pam['prc']->price;?>"><?php echo $ddiam?></option>
                   <?php } ?>

                </select>
            </p>
            <div class="price-quantity-tocart">
                <p class="product-item-price pull-left">

                    <span class="price-itself"><?php echo $prInfo['diametersAndMaterials'][0]['prc']->price;?></span>&nbsp;<span class="product-item-currency">руб</span>
                </p>
                <div class="quantity-tocart pull-right">
                    <input type="text" class="quantity" value="1"/>
                    <button class="to-cartnew">В корзину</button>
                </div>
                <div class="clear0"></div>
            </div>
        </div><!--.product-item-content-->
        <div class="clear0"></div>
    </div><!--.product-item-->

    <div class="clear5"></div>

        <?php } ?>



</div><!--#content-->
<div class=clear0></div>

</div><!--content-left-menu-->
<div class=clear0></div>