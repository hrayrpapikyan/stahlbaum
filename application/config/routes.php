<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['category/(:num)/(:any)/(:any)'] = 'front/category/$1/$2';
$route['category_only_diameters/(:num)/(:any)'] = 'front/category_only_diameters/$1';
$route['category_simple/(:num)/(:any)'] = 'front/category_simple/$1';
$route['supercategory/(:any)/(:any)'] = 'front/supercategory/$1';
$route['o_nas.html'] = 'front/o_nas';
$route['pokupatelyam.html'] = 'front/pokupatelyam';
$route['dostavka_i_oplata.html'] = 'front/dostavka_i_oplata';
$route['kontakti.html'] = 'front/kontakti';
$route['korzina_1.html'] = 'front/korzina_1';
$route['korzina_2.html'] = 'front/korzina_2';
$route['development.html'] = 'front/development';
$route['poisk.html'] = 'front/poisk';

$route['default_controller'] = "front";

$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */