jQuery(document).ready(function() {

    if($('.last-menu-item').hasClass('active-item')){
        $('#header-slider-container').addClass('wh-bg');
    }
    else{

    $('.last-menu-item').hover(function(){
        $('#header-slider-container').addClass('wh-bg');
    },
        function(){
            $('#header-slider-container').removeClass('wh-bg');
    })
    }

getOrInitializeCokies();
var headerCartContent  = countCartAndQuantity();
$('#hearder-cart-content-price').text(headerCartContent.cartTotal);
$('#hearder-cart-content-quantity').text(headerCartContent.cartQuantity);

    jQuery.fn.ForceNumericOnly =
        function()
        {
            return this.each(function()
            {
                $(this).keydown(function(e)
                {
                    var key = e.charCode || e.keyCode || 0;
                    // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                    // home, end, period, and numpad decimal
                    return (
                        key == 8 ||
                            key == 9 ||
                            key == 13 ||
                            key == 46 ||
                            key == 110 ||
                            key == 190 ||
                            (key >= 35 && key <= 40) ||
                            (key >= 48 && key <= 57) ||
                            (key >= 96 && key <= 105));
                });
            });
        };



    /*
    cart 1-st step scripts
     */

    // apply Numeric only control
    $('.quantity-td-input').ForceNumericOnly();

    //counts, collects and keep in cookies all results in cart



    $("a.product-item-popup").fancybox({
        'titleShow'  : false
    });


    $('.product-item-select select').change(function(){
//        alert($(this).find('option:selected').data('price'));
        if(parseInt($(this).find('option:selected').data('price'))){
       $(this).parent().parent().find('.price-itself').text($(this).find('option:selected').data('price'));
            $(this).parent().parent().find('.product-item-currency').html('').text('руб');
            $(this).parent().parent().find('.to-cart').css('visibility','visible');
            $(this).parent().parent().find('.quantity').css('visibility','visible');
        }
        else{
            $(this).parent().parent().find('.price-itself').text('Нет в наличии');
            $(this).parent().parent().find('.product-item-currency').html('<small style="color:black;">Выберите пожалуйстa другой диаметр, или материал</small>');
            $(this).parent().parent().find('.to-cart').css('visibility','hidden');
            $(this).parent().parent().find('.quantity').css('visibility','hidden');

        }


    })


    $('#reset-contact').click(function(e){
        e.preventDefault();
        $('.contact-form input').val('');
        $('.contact-form textarea').val('');
    })


});

function consolelogcookie(){
    var jj=$.parseJSON($.cookie('data'));
    console.log(jj);
}
//end document.ready
function toggleAddedToCart(){
    $('#to-cart-success').fadeIn('slow').fadeOut('slow');
}


  function getOrInitializeCokies(){
  if (typeof $.cookie('data') == 'undefined' || $.cookie('data')===null){
  var data ={};
  data.products=[];
  data.productsSimple=[];
  data.productsnew=[];
  data.shipping=0;
  data.payment=0;
  data.paymentPrice = 0;
  data.shippingPrice = 0;
  $.cookie('data',JSON.stringify(data),{ expires: 100, path:'/' });
     consolelogcookie();
  }
  else{
      consolelogcookie();
  }
  }

   function countCartAndQuantity(){
   var jj = $.parseJSON($.cookie('data'));
   var prodInCards = jj.products;
   var prodInCardsSimple = jj.productsSimple;
       var prodInCardsnew =  jj.productsnew;
       if (prodInCards.length==0 && prodInCardsSimple.length==0 && prodInCardsnew.length==0){
   return new function(){
   this.cartTotal = 0;
   this.cartQuantity = 0;
   }; 
   }
   else{
   var quant=0,totalPrice=0;
   $.each(prodInCards, function( index, value ) {
   quant+=parseInt(value.quantity);
   totalPrice+=parseInt(value.priceTotal);
    });

       $.each(prodInCardsnew, function( index, value ) {
           quant+=parseInt(value.quantity);
           totalPrice+=parseInt(value.priceTotal);
       });

       $.each(prodInCardsSimple, function( index, value ) {
           quant+=parseInt(value.quantity);
           totalPrice+=parseInt(value.priceTotal);
    });
      // alert(parseInt(jj.shippingPrice));
       totalPrice+=parseInt(jj.shippingPrice)+parseInt(jj.paymentPrice);
	return new function(){
   this.cartTotal = totalPrice;
   this.cartQuantity = quant;
   }; 
	
   }
  
   }

   $(document).on('click','.to-cart',function(){
   var drpd = $(this).parent().parent().parent().find('.product-item-select select option:selected');
   //alert(drpd.data('product'));
   var quantity = parseInt($(this).parent().find('.quantity').val());
    var jj = $.parseJSON($.cookie('data'));
       var allreadyInList=false;
       $.each(jj.products, function( index, value ) {
           if(this.product_id==drpd.data('product') && this.material_id==drpd.data('material') && this.diameter==drpd.data('diameter')){
               this.quantity=parseInt(this.quantity) + quantity;
               this.priceTotal=this.quantity*drpd.data('price');
               allreadyInList=true;
               return false;
           }
       });
       if(!allreadyInList)
   jj.products.push({'product_id':drpd.data('product'),'material_id':drpd.data('material'),'diameter':drpd.data('diameter'),'quantity':quantity,'priceTotal':quantity*drpd.data('price'),'pricePerItem':drpd.data('price')});
   $.cookie('data',JSON.stringify(jj),{ expires: 100, path:'/' });
       toggleAddedToCart();
       var headerCartContent  = countCartAndQuantity();
   $('#hearder-cart-content-price').text(headerCartContent.cartTotal);
$('#hearder-cart-content-quantity').text(headerCartContent.cartQuantity);
       consolelogcookie();
   })


$(document).on('click','.to-cartnew',function(){
    var drpd = $(this).parent().parent().parent().find('.product-item-select select option:selected');
    var quantity = parseInt($(this).parent().find('.quantity').val());
    var jj = $.parseJSON($.cookie('data'));
    var allreadyInList=false;
    $.each(jj.productsnew, function( index, value ) {
        if(this.product_id==drpd.data('product')  && this.diameter==drpd.data('diameter')){
            this.quantity=parseInt(this.quantity)+quantity;
            this.priceTotal=this.quantity*drpd.data('price');
            allreadyInList=true;
            return false;
        }
    });
    if(!allreadyInList)
        jj.productsnew.push({'product_id':drpd.data('product'),'diameter':drpd.data('diameter'),'quantity':quantity,'priceTotal':quantity*drpd.data('price'),'pricePerItem':drpd.data('price')});
    $.cookie('data',JSON.stringify(jj),{ expires: 100, path:'/' });
    toggleAddedToCart();
    var headerCartContent  = countCartAndQuantity();
    $('#hearder-cart-content-price').text(headerCartContent.cartTotal);
    $('#hearder-cart-content-quantity').text(headerCartContent.cartQuantity);
    consolelogcookie();
})


$(document).on('click','.to-cart-simple',function(){
    var quantity = parseInt($(this).parent().find('.quantity_simple').val());
    var jj = $.parseJSON($.cookie('data'));
    var _this=$(this);
    var allreadyInList=false;
    $.each(jj.productsSimple, function( index, value ) {
        if(this.product_id==_this.data('product')){
            this.quantity+=quantity;
            this.priceTotal=this.quantity*this.pricePerItem;
            allreadyInList=true;
            return false;
        }
    });
    if(!allreadyInList)
    jj.productsSimple.push({'product_id':$(this).data('product'),'quantity':quantity,'priceTotal':quantity*$(this).data('price'),'pricePerItem':$(this).data('price')});
    $.cookie('data',JSON.stringify(jj),{ expires: 100, path:'/' });
    toggleAddedToCart();
    var headerCartContent  = countCartAndQuantity();
    $('#hearder-cart-content-price').text(headerCartContent.cartTotal);
    $('#hearder-cart-content-quantity').text(headerCartContent.cartQuantity);
    consolelogcookie();
})





