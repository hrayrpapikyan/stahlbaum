tinymce.init({
    plugins: "textcolor",
    toolbar: "forecolor backcolor",
    selector: "textarea.tiny-custom"
});


//delete category - diameter relation  on right click
$(document).on('click','.delete-relation',function(e){
    var _this=$(this);
    $.post( "/admin/deleterelation", {relation_id:$(this).data('id'), diamt:$(this).parent().find('input[type="text"]').val()}, function( data ) {
        _this.parent().remove();
    });
})

$(document).on('click','.delete-relation-new',function(e){
    var _this=$(this);
    $.post( "/admin/deleterelationnew", {relation_id:$(this).data('id'), diamt:$(this).parent().find('input[type="text"]').val()}, function( data ) {
        _this.parent().remove();
    });
})

//delete category - diameter relation  on right click
$(document).on('click','.add-diameter',function(e){
$(this).parent().parent().append(
    ' <div class="form-group">'+
    '<input type="text" name="diameters[]" value="" class="form-control"> '+
    '<div>'
);
})


//delete category on right click
$(document).on('click','.product-link',function(e){
    e.preventDefault();
    var _this=$(this);
    $.post( "/admin/deleteProduct", {product_id:$(this).data('product')}, function( data ) {
        location.reload();
    });
})


//delete category on right click
$(document).on('click','.product-linknew',function(e){
    e.preventDefault();
    var _this=$(this);
    $.post( "/admin/deleteproductnew", {product_id:$(this).data('product')}, function( data ) {
        location.reload();
    });
})


//delete slider image on right click
$(document).on('contextmenu','.slides',function(e){
    e.preventDefault();
    var _this=$(this);
    $.post( "/admin/deleteslider", {slider_id:$(this).data('id')}, function( data ) {
        _this.remove();
    });
})

$(document).on('click','.category-link',function(e){
    e.preventDefault();
    var _this=$(this);
    $.post( "/admin/deleteCategory", {category_id:$(this).data('category')}, function( data ) {
        location.reload();
    });
})

$(document).on('click','.category-link-new',function(e){
    e.preventDefault();
    var _this=$(this);
    $.post( "/admin/deletecategorynew", {category_id:$(this).data('category')}, function( data ) {
        location.reload();
    });
})

$(document).on('click','.category-simple-link',function(e){
    e.preventDefault();
   // alert(123);
    var _this=$(this);
    $.post( "/admin/deletecategorysimple", {category_id:$(this).data('category')}, function( data ) {
        location.reload();
    });
})

$(document).on('click','.product-link-simple',function(e){
    e.preventDefault();
    var _this=$(this);
    $.post( "/admin/deleteproductsimple", {product_id:$(this).data('product')}, function( data ) {
        location.reload();
    });
})

$(document).on('click','.supercategory-link',function(e){
    e.preventDefault();
    var _this=$(this);
    $.post( "/admin/deletesupercategory", {supercategory_id:$(this).data('supercategory')}, function( data ) {
        location.reload();
    });
})


/*function addMaterial(){
    var materialHTML =
        '<div class="form-group" style="border:1px solid blue;border-radius:10px;padding:5px;">'+
        '<label>Длинное название</label>'+
        '<input type="text" name="mataerial_long[]" value="" class="form-control">'+
        '<label>Короткое название</label>'+
        '<input type="text" name="material_short[]" value="" class="form-control">';

    $( ".diam-checkbox" ).each(function( index ) {
        materialHTML+='<label data-diameter="'+$(this).val()+'"';
        if(!$(this).is(':checked')) {materialHTML+=' style="display:none" ';}
        materialHTML+= '>Цена - '+$(this).val()+'</label>'+
            '<input type="text" ';
        if(!$(this).is(':checked')) {materialHTML+=' style="display:none" disabled';}
        materialHTML+=' name="material_price[]" value="" class="form-control" data-diameter="'+$(this).val()+'">';

    });

   *//*     '<label>Цена</label>'+
        '<input type="text" name="material_price[]" value="" class="form-control">';*//*

         materialHTML+='<label class="remove_material"><i class="fa fa-minus-circle fa-fw" style="color:RED"></i> Удалить</label>'+
        '</div>';

    $('#materials_area').append(materialHTML);
}*/

function addMaterial(){
    $('#materials_area').append(materialBody);
}


$(document).on('click','.add-material',function(){
    addMaterial();
})

$(document).on('click','.remove_material',function(){
    $(this).parent().remove();
})

$(document).on('click','.remove_material_edit',function(){

    var _this=$(this);
    $.post( "/admin/removematerialedit", {product_id:$('.keeper').data('product_id'), material_id:$(this).parent().find('.material-id-keeper').val()}, function( data ) {
        _this.parent().remove();
    });

})


$(document).on('click','.add-material-edit',function(){

    var _this=$(this);
    $.post( "/admin/getBodyForNewMaterialEdit", {product_id:$('.keeper').data('product_id')}, function( data ) {
       // _this.parent().remove();
          $('#materials_area').append(data);
    });

})